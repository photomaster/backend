# PhotoMaster Backend 

![build status](https://concourse.photomaster.rocks/api/v1/teams/main/pipelines/backend/badge "ci build status")

## Install

1. Copy `.env.default` to `.env` and adjust settings.

2. Copy `.env.default` to `.env.imsim` and adjust settings (`POSTGRES_*`).

3. Run `docker-compose build`

## Run
```shell script
docker-compose up
```

## Run test suite
```shell script
make test # or
docker-compose exec web bash -c "python ./manage.py test"
```

## Create admin user
```shell script
docker-compose exec web bash
> python ./manage.py createsuperuser
```

## Help and further information

- API documentation: http://localhost:8080/api/
- Admin panel: http://localhost:8080/admin/
