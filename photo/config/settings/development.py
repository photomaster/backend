from .base import *

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': os.getenv('POSTGRES_DB'),
        'USER': os.getenv('POSTGRES_USER'),
        'HOST': os.getenv('POSTGRES_HOST', 'pg'),
        'PASSWORD': os.getenv('POSTGRES_PASSWORD', ''),
        'PORT': 5432
    }
}
