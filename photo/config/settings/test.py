import logging

from .base import *

logging.disable(logging.WARNING)

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': os.getenv('POSTGRES_DB'),
        'USER': os.getenv('POSTGRES_USER'),
        'HOST': os.getenv('POSTGRES_HOST', 'pg'),
        'PASSWORD': os.getenv('POSTGRES_PASSWORD', ''),
        'PORT': 5432
    }
}

SESSION_ENGINE = 'django.contrib.sessions.backends.file'

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    }
}
