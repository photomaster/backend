# patch grpc libraries to be compatible with gevent.
# without the patch the grpc request hangs indefinitely.
# @see https://github.com/grpc/grpc/issues/4629#issuecomment-199388687
import grpc.experimental.gevent as grpc_gevent
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

from .base import *

grpc_gevent.init_gevent()

INSTALLED_APPS += ['gunicorn', 'django_prometheus']

MIDDLEWARE = ['django_prometheus.middleware.PrometheusBeforeMiddleware'] + \
    MIDDLEWARE + \
             ['django_prometheus.middleware.PrometheusAfterMiddleware']

PROMETHEUS_EXPORT_MIGRATIONS = False

ALLOWED_HOSTS += ["api.photomaster.rocks", "photomaster.rocks"]

CORS_ORIGIN_WHITELIST = [
    "https://photo.multimediatechnology.at",
    "https://photomaster.rocks"
]

DATABASES = {
    'default': {
        'ENGINE': 'django_db_geventpool.backends.postgis',
        'NAME': os.getenv('POSTGRES_DB'),
        'USER': os.getenv('POSTGRES_USER'),
        'HOST': os.getenv('POSTGRES_HOST', 'pg'),
        'PASSWORD': os.getenv('POSTGRES_PASSWORD', ''),
        'PORT': 5432,
        'ATOMIC_REQUESTS': False,
        'CONN_MAX_AGE': 0,
        'OPTIONS': {
            'MAX_CONNS': 20,
            'REUSE_CONNS': 10
        }
    }
}

sentry_sdk.init(
    dsn=os.getenv("SENTRY_URL"),
    integrations=[DjangoIntegration()],
    traces_sample_rate=1.0,
    send_default_pii=False
)
