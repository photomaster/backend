# patch grpc libraries to be compatible with gevent.
# without the patch the grpc request hangs indefinitely.
# @see https://github.com/grpc/grpc/issues/4629#issuecomment-199388687
import grpc.experimental.gevent as grpc_gevent

from .base import *

grpc_gevent.init_gevent()

INSTALLED_APPS += ['gunicorn']
INSTALLED_APPS.remove('django_elasticsearch_dsl')

CORS_ORIGIN_WHITELIST = [
    "https://photomaster.rocks",
    "http://127.0.0.1:3000",
]

DATABASES = {
    'default': {
        'ENGINE': 'django_db_geventpool.backends.postgis',
        'NAME': os.getenv('POSTGRES_DB'),
        'USER': os.getenv('POSTGRES_USER'),
        'HOST': os.getenv('POSTGRES_HOST', 'pg'),
        'PASSWORD': os.getenv('POSTGRES_PASSWORD', ''),
        'PORT': 5432,
        'ATOMIC_REQUESTS': False,
        'CONN_MAX_AGE': 0,
        'OPTIONS': {
            'MAX_CONNS': 20,
            'REUSE_CONNS': 10
        }
    }
}
