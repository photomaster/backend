from django.conf.urls import url, include
from rest_framework import routers
from rest_framework_simplejwt.views import (
    TokenRefreshView,
    TokenObtainPairView,
)

from .views import (
    UserViewSet,
    PhotoViewSet,
    WeatherStateViewSet,
    LocationViewSet,
    LabelViewSet,
    ProfileViewSet,
    AuthView,
    FavoritePhotosViewSet,
    PhotoDataViewSet,
)

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r"users", UserViewSet)
router.register(r"photos", PhotoViewSet)
router.register(r"weatherState", WeatherStateViewSet)
router.register(r"locations", LocationViewSet)
router.register(r"labels", LabelViewSet)
router.register(r"profile/favorites", FavoritePhotosViewSet, "favoritephoto")
router.register(r"photosData", PhotoDataViewSet)
router.register(r"profile", ProfileViewSet, "profile")

urlpatterns = [
    url(r"^auth/register/", AuthView.as_view(), name="auth_register"),
    url(r"^auth/refresh/", TokenRefreshView.as_view(), name="auth_token_refresh"),
    url(r"^auth/", TokenObtainPairView.as_view(), name="auth_token_obtain_pair"),
    url(r"^", include(router.urls)),
]
