from datetime import datetime
from unittest.mock import MagicMock

from django.test import TestCase, RequestFactory
from rest_framework.views import APIView

from photo.api.filters import PhotoFilterBackend
from photo.locations.models import Photo, FavoritePhoto


class PhotoFilterBackendTest(TestCase):
    def setUp(self) -> None:
        self.photo1 = Photo(guid="1", originalCreationDate=datetime.fromisoformat("2020-06-30 00:00+01:00"))
        self.photo2 = Photo(guid="2", originalCreationDate=datetime.fromisoformat("2009-06-30 00:00+01:00"))
        self.photo3 = Photo(guid="3", originalCreationDate=None)
        Photo.objects.bulk_create(
            [
                self.photo1,
                self.photo2,
                self.photo3,
            ]
        )
        FavoritePhoto.objects.bulk_create(
            [
                FavoritePhoto(photo=self.photo1),
                FavoritePhoto(photo=self.photo1),
                FavoritePhoto(photo=self.photo2),
            ]
        )

        self.request_factory = RequestFactory()
        self.filter_backend = PhotoFilterBackend()
        self.view = MagicMock()
        self.view.ordering_fields = ["id", "originalCreationDate", "popularity"]

    def __initialize_request(self, request):
        return APIView().initialize_request(request)

    def test_filter_queryset__popularity_desc(self):
        request = self.__initialize_request(self.request_factory.get("/api/photos/?ordering=popularity"))

        queryset = self.filter_backend.filter_queryset(request, Photo.objects.all(), None)

        self.assertEqual(queryset[0].guid, "1", "the photo having the most likes should be at the start")
        self.assertEqual(queryset[2].guid, "3", "the photo having the least likes should be at the end")

    def test_filter_queryset__popularity_asc(self):
        request = self.__initialize_request(self.request_factory.get("/api/photos/?ordering=-popularity"))

        queryset = self.filter_backend.filter_queryset(request, Photo.objects.all(), None)

        self.assertEqual(queryset[0].guid, "1", "popularity should always be ordered descendingly")
        self.assertEqual(queryset[2].guid, "3", "popularity should always be ordered descendingly")

    def test_filter_queryset__generic_desc(self):
        request = self.__initialize_request(self.request_factory.get("/api/photos/?ordering=originalCreationDate"))

        queryset = self.filter_backend.filter_queryset(request, Photo.objects.all(), self.view)

        self.assertEqual(queryset[0].guid, "2", "should be ordered by originalCreationDate ascendingly")

    def test_filter_queryset__generic_asc(self):
        request = self.__initialize_request(self.request_factory.get("/api/photos/?ordering=-originalCreationDate"))

        queryset = self.filter_backend.filter_queryset(request, Photo.objects.all(), self.view)

        self.assertEqual(queryset[0].guid, "1", "should be ordered by originalCreationDate descendingly")
