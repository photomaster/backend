import json
from datetime import datetime
from unittest import mock
from unittest.mock import patch

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.test.client import encode_multipart
from django.utils.translation import gettext as _
from rest_framework import status

from photo.locations.models import Photo, WeatherState, PhotoData
from .test import _generate_photo_file, _vision_api_mock_response


def _setup_weather_api_mock(api_mock):
    api_mock.return_value = {
        "weather": [{"id": 800, "main": "Clear", "description": "clear sky", "icon": "01d"}],
        "main": {
            "temp": 42.11,
        },
        "sys": {"sunrise": 1560343627, "sunset": 1560396563},
        "id": 420006353,
        "name": "Mountain View",
    }


class RegressionTest(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create(username="user", displayName="Otto Normal", email="test@test.de")

    def tearDown(self):
        self.user.delete()

    @patch(
        "photo.locations.service.photo._get_vision_api_response",
        mock.MagicMock(return_value=_vision_api_mock_response("adult")),
    )
    def test_upload_adult_content(self):
        self.client.force_login(self.user)

        with open(".test/images/palpatine.jpg", "rb") as f:
            request_data = {
                "name": "image containing adult content",
                "description": "Go to horny jail! *bonk*",
                "image": f,
                "visibility": 3,
            }
            response = self.client.post("/api/photos/", data=request_data)
            data = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("detail", data)
        self.assertEqual(data["detail"], _("image.upload.adult_content"))
        self.assertEqual(
            len(Photo.objects.filter(name="image containing adult content")),
            0,
            "if photo violates content policy it should not be saved.",
        )

    @patch(
        "photo.locations.service.photo._get_vision_api_response",
        mock.MagicMock(return_value=_vision_api_mock_response("violence")),
    )
    def test_upload_violent_content(self):
        with open(".test/images/palpatine.jpg", "rb") as f:
            request_data = {
                "name": "image containing violent content",
                "description": "very violent content",
                "image": f,
                "visibility": 3,
            }
            self.client.force_login(self.user)
            response = self.client.post("/api/photos/", data=request_data)
            data = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("detail", data)
        self.assertEqual(data["detail"], _("image.upload.violent_content"))
        self.assertEqual(
            len(Photo.objects.filter(name="image containing violent content")),
            0,
            "if photo violates content policy it should not be saved.",
        )

    @patch("photo.locations.tasks.photo_post_create_async")
    def test_upload_of_photos_always_creates_photo_data(self, post_create_async_hook):

        self.client.force_login(self.user)
        request_data = {
            "name": "image without exifdata",
            "labels": [],
            "image": _generate_photo_file("test.png"),
            "originalCreationDate": "2020-09-21T18:13:49Z",
            "visibility": 3,
        }
        r = self.client.post(
            "/api/photos/",
            data=encode_multipart("bound", request_data),
            content_type="multipart/form-data; boundary=bound",
        )

        self.assertEqual(r.status_code, status.HTTP_201_CREATED)
        photo = Photo.objects.get(pk=json.loads(r.content)["id"])
        self.assertIsInstance(photo.photodata, PhotoData)

    @patch("photo.locations.tasks.photo_post_create_async")
    @patch("photo.locations.service.weather._fetch_api_data")
    def test_photo_upload_creates_weatherstate__photo_shot_in_past(self, _fetch_api_data, _):
        _setup_weather_api_mock(_fetch_api_data)
        self.client.force_login(self.user)

        r = self.client.post(
            "/api/photos/",
            data=encode_multipart(
                "bound",
                {
                    "name": "image without exifdata",
                    "labels": [],
                    "originalCreationDate": "2005-09-21T18:13:49Z",
                    "visibility": 3,
                },
            ),
            content_type="multipart/form-data; boundary=bound",
        )
        self.assertEqual(r.status_code, status.HTTP_201_CREATED)
        photo = Photo.objects.get(pk=json.loads(r.content)["id"])
        self.assertIsInstance(
            photo.weatherAtCreation,
            WeatherState,
            msg="Uploaded photos shot in the past should be connected with a weatherState",
        )

    @patch("photo.locations.tasks.photo_post_create_async")
    @patch("photo.locations.service.weather._fetch_api_data")
    def test_photo_upload_creates_weatherstate__no_creation_date(self, _fetch_api_data, _):
        _setup_weather_api_mock(_fetch_api_data)
        self.client.force_login(self.user)

        r = self.client.post(
            "/api/photos/",
            data=encode_multipart(
                "bound",
                {
                    "name": "image without exifdata",
                    "labels": [],
                    "visibility": 3,
                },
            ),
            content_type="multipart/form-data; boundary=bound",
        )
        self.assertEqual(r.status_code, status.HTTP_201_CREATED)
        photo = Photo.objects.get(pk=json.loads(r.content)["id"])
        self.assertIsInstance(
            photo.weatherAtCreation,
            WeatherState,
            msg="Uploaded photos without a creation date specified should be connected with a weatherState",
        )

    @patch("photo.locations.tasks.photo_post_create_async")
    @patch("photo.locations.service.weather._fetch_api_data")
    def test_photo_upload_creates_weatherstate__shot_now__has_geo_coordinates(self, _fetch_api_data, _):
        _setup_weather_api_mock(_fetch_api_data)
        self.client.force_login(self.user)

        r = self.client.post(
            "/api/photos/",
            data=encode_multipart(
                "bound",
                {
                    "name": "image without exifdata",
                    "labels": [],
                    "originalCreationDate": datetime.today(),
                    "geo": '{"type": "Point","coordinates": [13.0468,47.801186]}',
                    "visibility": 3,
                },
            ),
            content_type="multipart/form-data; boundary=bound",
        )
        self.assertEqual(r.status_code, status.HTTP_201_CREATED)

        photo = Photo.objects.get(pk=json.loads(r.content)["id"])
        self.assertIsInstance(
            photo.weatherAtCreation,
            WeatherState,
            msg="Uploaded photos that have been shot today should be connected with a weatherState",
        )
        _fetch_api_data.assert_called_once()
