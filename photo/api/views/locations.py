from django.contrib.gis.geos import Point
from django.http import JsonResponse
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from photo.api.serializers import LocationSerializer
from photo.api.views.photo import naive_km_to_degrees
from photo.locations.models import Location
from photo.locations.service.location import location_name_to_gps


class LocationViewSet(viewsets.ModelViewSet):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer

    def list(self, request, **kwargs):
        query_set = self.queryset
        if "lat" in request.query_params and "long" in request.query_params:
            lat = float(request.query_params["lat"])
            long = float(request.query_params["long"])
            distance = float(request.query_params["distance"]) if "distance" in request.query_params else 25

            geo_point = Point(long, lat)
            query_set = query_set.filter(centroid__dwithin=(geo_point, naive_km_to_degrees(distance)))

        serializer = self.serializer_class(query_set, many=True, context={"request": self.request})
        return Response(serializer.data)

    @action(detail=False, methods=["get"])
    def location_name_search(self, request):
        if "q" not in request.query_params:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        try:
            (lat, long) = location_name_to_gps(request.query_params["q"])
            return JsonResponse(data={"lat": lat, "long": long})
        except RuntimeError:
            return JsonResponse(status=status.HTTP_404_NOT_FOUND, data={})
