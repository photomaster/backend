import json

from django.contrib.auth import get_user_model
from django.test import TestCase
from rest_framework import status

from photo.locations.models import Photo, FavoritePhoto


class FavoritePhotosViewSetTest(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create(username="user", displayName="Otto Normal", email="test@test.de")
        self.photo = Photo.objects.create(user=self.user, guid="1", visibility=Photo.Visibility.PUBLIC)

    def tearDown(self):
        self.user.delete()
        self.photo.delete()

    def test_create__unauthenticated(self):
        response = self.client.post(
            "/api/profile/favorites/", data={"photo": {"id": self.photo.id}}, content_type="application/json"
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create(self):
        self.client.force_login(self.user)
        response = self.client.post(
            "/api/profile/favorites/", data={"photo": {"id": self.photo.id}}, content_type="application/json"
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        data = json.loads(response.content)
        favorite = FavoritePhoto.objects.get(id=data["id"])
        self.assertIsNotNone(favorite, "should create a favorite photo objects if it doesn't yet")
        self.assertEqual(favorite.photo, self.photo)
        self.assertEqual(favorite.user, self.user)

    def test_create__empty_photo(self):
        self.client.force_login(self.user)

        response = self.client.post("/api/profile/favorites/", data={"photo": {}}, content_type="application/json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create__missing_body(self):
        self.client.force_login(self.user)

        response = self.client.post("/api/profile/favorites/", content_type="application/json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create__invalid_photo(self):
        self.client.force_login(self.user)

        response = self.client.post(
            "/api/profile/favorites/", data={"photo": {"id": "abcd"}}, content_type="application/json"
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create__photo_not_existing(self):
        self.client.force_login(self.user)

        response = self.client.post(
            "/api/profile/favorites/", data={"photo": {"id": 1234}}, content_type="application/json"
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
