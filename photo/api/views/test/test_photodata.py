from unittest.mock import patch

from django.contrib.auth import get_user_model
from django.test import TestCase
from rest_framework import status

from photo.locations.models import Photo, PhotoData


class PhotoDataViewSetTest(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create(username="user", displayName="Otto Normal", email="test@test.de")
        self.photo = Photo.objects.create(user=self.user, guid="1", visibility=Photo.Visibility.PUBLIC)
        self.photodata = PhotoData.objects.create(user=self.user, photo=self.photo, make="Apple", model="iPhone 5s")

    def tearDown(self):
        self.photo.delete()
        self.photodata.delete()
        self.user.delete()

    @patch("photo.locations.tasks.photodata_post_update_async")
    def test_update_unauthorized(self, photodata_post_update_async):
        other_user = get_user_model().objects.create(username="another_user", email="another@user.de")
        self.client.force_login(other_user)

        response = self.client.put(
            "/api/photosData/{}/".format(self.photodata.id),
            data={"make": "Samsung", "model": "Galaxy S6"},
            content_type="application/json",
        )

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        photodata_post_update_async.assert_not_called()

    @patch("photo.locations.tasks.photodata_post_update_async")
    def test_update_unauthenticated(self, photodata_post_update_async):
        response = self.client.put(
            "/api/photosData/{}/".format(self.photodata.id),
            data={"make": "Samsung", "model": "Galaxy S6"},
            content_type="application/json",
        )

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        photodata_post_update_async.assert_not_called()

    @patch("photo.locations.tasks.photodata_post_update_async")
    def test_update(self, photodata_post_update_async):
        self.client.force_login(self.user)

        response = self.client.put(
            "/api/photosData/{}/".format(self.photodata.id),
            data={"make": "Samsung", "model": "Galaxy S6"},
            content_type="application/json",
        )
        self.photodata.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(self.photodata.make, "Samsung", "should update make field of photodata object")
        self.assertEqual(self.photodata.model, "Galaxy S6", "should update model field of photodata object")
        photodata_post_update_async.assert_called_once_with(self.photodata)
