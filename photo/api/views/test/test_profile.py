import json
from unittest.mock import patch

from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.test import TestCase
from django.test.client import encode_multipart
from rest_framework import status

from photo.locations.models import Photo
from . import _generate_photo_file


class ProfileViewTest(TestCase):
    def setUp(self):
        self.user = get_user_model()(username="user", displayName="Otto Normal", email="test@test.de")
        self.user.set_password("1234")
        self.user.save()

        Photo.objects.create(user=self.user, guid="1", visibility=Photo.Visibility.PUBLIC)
        Photo.objects.create(user=self.user, guid="2", visibility=Photo.Visibility.PRIVATE)
        Photo.objects.create(user=self.user, guid="3", visibility=Photo.Visibility.DRAFT)

    def testProfilePage__logged_in(self):
        self.client.force_login(self.user)

        response = self.client.get("/api/profile/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        data = json.loads(response.content)
        self.assertEqual(data["username"], "user")
        self.assertEqual(data["displayName"], "Otto Normal")
        self.assertEqual(data["email"], "test@test.de")
        self.assertFalse("password" in data, "should not expose password!")

    def testProfilePage__photo_list__logged_in(self):
        self.client.force_login(self.user)

        response = self.client.get("/api/profile/photos/")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            len(json.loads(response.content)["results"]), 3, "should return all photos regardless of visibility state"
        )

    def testProfilePage__not_authenticated(self):
        response = self.client.get("/api/profile/")
        self.assertEqual(
            response.status_code, status.HTTP_401_UNAUTHORIZED, "profile should not be accessible without valid token"
        )

    def testProfilePage__photo_list__not_authenticated(self):
        response = self.client.get("/api/profile/photos/")
        self.assertEqual(
            response.status_code,
            status.HTTP_401_UNAUTHORIZED,
            "personal photos should not be accessible without valid token",
        )

    @patch("django.contrib.auth.password_validation.validate_password", return_value=True)
    def testProfilePut(self, _):
        self.client.force_login(self.user)

        data = {
            "displayName": "Darth Vader",
            "password": "dark-side",
            "description": "most impressive!",
            "email": "darth.vader@galactic-empire.com",
            "avatarImage": _generate_photo_file("avatar"),
            "bannerImage": _generate_photo_file("banner"),
            "username": "vader",
        }

        response = self.client.put(
            "/api/profile/", data=encode_multipart("bound", data), content_type="multipart/form-data; boundary=bound"
        )
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK,
            "should update the user object with new data provided by the request body",
        )
        returned_data = json.loads(response.content)
        self.user.refresh_from_db()

        self.assertEqual(data["displayName"], returned_data["displayName"])
        self.assertEqual(data["email"], returned_data["email"])
        self.assertEqual(data["username"], returned_data["username"])
        self.assertEqual(data["description"], returned_data["description"])
        self.assertRegexpMatches(returned_data["avatarImage"], r"media/avatars/avatar(_[\w\d]+)*.png$")
        self.assertRegexpMatches(returned_data["bannerImage"], r"media/banners/banner(_[\w\d]+)*.png$")

        self.assertFalse("password" in returned_data)
        self.assertTrue(self.user.check_password(data["password"]), "login with new password should be possible")

    def testProfileUpdate__not_authenticated(self):
        response = self.client.put("/api/profile/")
        self.assertEqual(
            response.status_code, status.HTTP_401_UNAUTHORIZED, "profile should not be accessible without valid token"
        )

    def testProfileUpdate__not_authenticated__parameters_are_set(self):
        response = self.client.put("/api/profile/", data={"displayName": "Darth Vader"})
        self.assertEqual(
            response.status_code,
            status.HTTP_401_UNAUTHORIZED,
            "profile should not be accessible without valid token if parameters are set",
        )

    def testProfilePatch__simple_fields(self):
        self.client.force_login(self.user)

        data = {
            ("displayName", "Darth Vader"),
            ("description", "I find your lack of faith disturbing."),
            ("email", "darth.vader@galactic-empire.com"),
            ("username", "vader"),
        }

        for case in data:
            with self.subTest(case):
                key, value = case

                response = self.client.patch(
                    "/api/profile/",
                    data=encode_multipart("bound", {key: value}),
                    content_type="multipart/form-data; boundary=bound",
                )
                returned_data = json.loads(response.content)

                self.assertEqual(response.status_code, status.HTTP_200_OK)
                self.assertEqual(value, returned_data[key], 'should successfully patch the property "' + key + '"')

    def testProfilePatch__image_fields(self):
        self.client.force_login(self.user)

        data = (
            ("avatarImage", _generate_photo_file("avatar")),
            ("bannerImage", _generate_photo_file("banner")),
        )

        for case in data:
            with self.subTest(case):
                key, value = case

                response = self.client.patch(
                    "/api/profile/",
                    data=encode_multipart("bound", {key: value}),
                    content_type="multipart/form-data; boundary=bound",
                )
                returned_data = json.loads(response.content)

                self.assertEqual(response.status_code, status.HTTP_200_OK)
                if key == "avatarImage":
                    self.assertRegexpMatches(returned_data["avatarImage"], r"media/avatars/avatar(_[\w\d]+)*.png$")
                elif key == "bannerImage":
                    self.assertRegexpMatches(returned_data["bannerImage"], r"media/banners/banner(_[\w\d]+)*.png$")

    @patch("django.contrib.auth.password_validation.validate_password", return_value=True)
    def testProfilePatch__password(self, _):
        new_password = "new_password"
        self.client.force_login(self.user)

        response = self.client.patch(
            "/api/profile/",
            data=encode_multipart("bound", {"password": "new_password"}),
            content_type="multipart/form-data; boundary=bound",
        )
        self.user.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(self.user.check_password(new_password), "login with new password should be possible")

    def testProfilePatch__not_authenticated(self):
        response = self.client.patch("/api/profile/")
        self.assertEqual(
            response.status_code, status.HTTP_401_UNAUTHORIZED, "profile should not be accessible without valid token"
        )

    def testProfilePatch__not_authenticated__paramaters_are_set(self):
        response = self.client.patch("/api/profile/", data={"displayName": "Darth Vader"})
        self.assertEqual(
            response.status_code,
            status.HTTP_401_UNAUTHORIZED,
            "profile should not be accessible without valid token if parameters are set",
        )

    @patch(
        "django_pwned_passwords.password_validation.PWNEDPasswordValidator.check_valid",
        side_effect=ValidationError("breached password"),
    )
    def test_profile_update_password__weak_password(self, password_validator):
        self.client.force_login(self.user)

        response = self.client.patch("/api/profile/", data={"password": "1234"}, content_type="application/json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, "should prevent setting a weak password")
        password_validator.assert_called()

    @patch("django_pwned_passwords.password_validation.PWNEDPasswordValidator.check_valid", return_value=True)
    def test_profile_update_password__only_if_password_changed(self, password_validator):
        self.client.force_login(self.user)

        self.client.patch(
            "/api/profile/", data={"displayName": "another display name"}, content_type="application/json"
        )

        password_validator.assert_not_called()
