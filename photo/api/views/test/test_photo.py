import json
from datetime import datetime
from unittest.mock import patch

from django.contrib.auth import get_user_model
from django.contrib.gis.geos import Point
from django.test import TestCase
from rest_framework import status

from photo.locations.models import Label, Photo, LabelScore, WeatherState, SolarData, PhotoData
from . import _get_solar_data_mock, _photo_map_fixtures


def _load_photos(client, path):
    response = client.get(path)
    data = json.loads(response.content)
    return response, data["results"] if "results" in data else data


class PhotoViewListParameterSetTest(TestCase):
    @patch("google.cloud.translate_v2.Client")
    def setUp(self, translate_client):
        def mock_translate(key, **kwargs):
            return {"translatedText": f"{key}_{kwargs['source_language']}->{kwargs['target_language']}"}

        translate_client.return_value.translate = mock_translate

        labels = (
            Label.objects.create(title="Star Wars"),
            Label.objects.create(title="Snow"),
            Label.objects.create(title="Car"),
        )

        for i in range(30):
            photo = Photo.objects.create(name=f"image-{i}", visibility=Photo.Visibility.PUBLIC, guid=i)
            label_score, _ = LabelScore.objects.get_or_create(label=labels[i % len(labels)], score=1.0, photo=photo)

    def test_list__offset(self):
        data = (
            (None, status.HTTP_200_OK, 25, "should use offset 0 if parameter is missing"),
            (10, status.HTTP_200_OK, 20, "should ignore skip first 10 results and return 20 photos"),
            (29, status.HTTP_200_OK, 1, "should ignore skip first 29 results and return 1 photo"),
            (-10, status.HTTP_200_OK, 25, "should fallback to 0 if parameter is invalid and return 25 results"),
            ("abc", status.HTTP_200_OK, 25, "should fallback to 0 if parameter is invalid and return 25 results"),
            ("10-foobar", status.HTTP_200_OK, 25, "should fallback to 0 if parameter is invalid and return 25 results"),
            (100, status.HTTP_200_OK, 0, "should return no results if offset is greater than photo count"),
        )

        for case in data:
            with self.subTest(case):
                offset, response_code, response_count, msg = case
                url = "/api/photos/" if offset is None else f"/api/photos/?offset={offset}"

                response, result = _load_photos(self.client, url)
                self.assertEqual(response.status_code, response_code)
                self.assertEqual(len(result), response_count, msg)

    def test_list__labels(self):
        data = (
            (None, status.HTTP_200_OK, 25, "if no valid label parameter is set ignore parameter"),
            ("", status.HTTP_200_OK, 25, "if no valid label parameter is set ignore parameter"),
            ("Star Wars", status.HTTP_200_OK, 10, "should only return photos with label 'Star Wars'"),
            (
                "Star%20Wars",
                status.HTTP_200_OK,
                10,
                "should correctly decode url encoded strings and return photos with label 'Star Wars'",
            ),
            (
                "Star Wars,Snow",
                status.HTTP_200_OK,
                20,
                "should return union of photos with label 'Star Wars' and 'Snow'",
            ),
            (
                "Star Wars,Snow,",
                status.HTTP_200_OK,
                20,
                "should ignore empty labels and return 'Star Wars' and 'Snow' photos",
            ),
            ("Car,IdontExist", status.HTTP_200_OK, 10, "should ignore labels that don't exist"),
        )

        for case in data:
            with self.subTest(case):
                labels, response_code, response_count, msg = case
                url = "/api/photos/" if labels is None else f"/api/photos/?labels={labels}"
                response, result = _load_photos(self.client, url)
                self.assertEqual(response.status_code, response_code)
                self.assertEqual(
                    len(result),
                    response_count,
                    msg,
                )

    def test_list__limit(self):
        data = (
            (None, status.HTTP_200_OK, 25, "if no limit parameter is set the api should return 25 results max"),
            (10, status.HTTP_200_OK, 10, "api should respect the limit parameter and return n results max"),
            (100, status.HTTP_200_OK, 25, "should not return more than 25 results regardless of the parameter"),
            ("", status.HTTP_200_OK, 25, "should ignore invalid parameter value 'None' and fallback to upper bound"),
            (-10, status.HTTP_200_OK, 25, "should ignore invalid parameter -10 and fallback to default limit"),
            ("foo", status.HTTP_200_OK, 25, "should ignore invalid parameter 'foo' and fallback to default limit"),
        )

        for case in data:
            with self.subTest(case):
                limit, response_code, response_count, msg = case
                url = "/api/photos/" if limit is None else f"/api/photos/?limit={limit}"
                response, result = _load_photos(self.client, url)
                self.assertEqual(response.status_code, response_code)
                self.assertEqual(
                    len(result),
                    response_count,
                    msg,
                )


class PhotoViewSetVisibilityTest(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create(
            email="test@test.de",
            username="foobar",
        )

        Photo.objects.create(name="public image of user", visibility=Photo.Visibility.PUBLIC, guid="1", user=self.user),
        Photo.objects.create(
            name="private image of user", visibility=Photo.Visibility.PRIVATE, guid="2", user=self.user
        )
        Photo.objects.create(name="draft image of user", visibility=Photo.Visibility.DRAFT, guid="3", user=self.user)
        Photo.objects.create(name="public image (other user)", visibility=Photo.Visibility.PUBLIC, guid="4")

    def test_list_visibility__unauthenticated_public(self):
        response, result = _load_photos(self.client, "/api/photos/")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(result), 2)
        self.assertEqual(result[0]["name"], "public image of user")
        self.assertEqual(result[1]["name"], "public image (other user)")

    def test_list_visibility__unauthenticated_public_of_user(self):
        response, result = _load_photos(self.client, f"/api/users/{self.user.pk}/photos/")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0]["name"], "public image of user")

    def test_list_visibility__authenticated_profile(self):
        self.client.force_login(self.user)

        response, result = _load_photos(self.client, "/api/profile/photos/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(result), 3, "an authenticated user should see all of his photos")


class PhotoViewSetTest(TestCase):
    @patch("photo.locations.tasks.photo_post_update_async")
    @patch("photo.locations.service.weather._fetch_api_data")
    def testCreation_update_weatherAtCreation(self, mock_fetch_api_data, mock_async_post_update_hook):
        mock_fetch_api_data.return_value = {
            "weather": [{"id": 800, "main": "Clear", "description": "clear sky", "icon": "01d"}],
            "main": {
                "temp": 42.11,
            },
            "sys": {"sunrise": 1560343627, "sunset": 1560396563},
            "id": 420006353,
            "name": "Mountain View",
        }

        user = get_user_model().objects.create(username="user")
        self.client.force_login(user)
        photo = Photo.objects.create(name="foobar", user=user)

        request_data = {
            "name": "foobar 123",
            "geo": '{"type": "Point","coordinates": [13.0468,47.801186]}',
            "visibility": Photo.Visibility.PUBLIC,
            "originalCreationDate": datetime.today(),
        }
        response = self.client.patch(f"/api/photos/{photo.id}/", data=request_data, content_type="application/json")
        photo.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        mock_fetch_api_data.assert_called_with(13.0468, 47.801186)
        self.assertIsInstance(photo.weatherAtCreation, WeatherState)
        self.assertEqual(photo.name, request_data["name"])
        self.assertEqual(photo.weatherAtCreation.temperature, 42.11)

    @patch("photo.locations.tasks.photo_post_create_async")
    def testCreation(self, mock_async_post_create_hook):
        user = get_user_model().objects.create(username="user")
        self.client.force_login(user)

        with open(".test/images/palpatine.jpg", "rb") as f:
            request_data = {
                "name": "upload test",
                "description": "It’s the ship that made the Kessel run in less than twelve parsecs.",
                "geo": '{"type": "Point","coordinates": [13.0468,47.801186]}',
                "labels": [],
                "image": f,
                "originalCreationDate": "2020-09-21T18:13:49Z",
                "visibility": 3,
            }
            response = self.client.post("/api/photos/", data=request_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        data = json.loads(response.content)
        self.assertEqual(request_data["name"], data["name"])
        self.assertEqual(request_data["description"], data["description"])
        self.assertEqual(json.loads(request_data["geo"]), data["geo"])
        self.assertEqual(request_data["originalCreationDate"], data["originalCreationDate"])
        self.assertEqual(request_data["visibility"], data["visibility"])

        photo = Photo.objects.get(pk=data["id"])
        self.assertIsNotNone(photo)

    @patch("photo.locations.tasks.photo_post_create_sync")
    @patch("photo.locations.tasks.photo_post_create_async")
    def testCreation__calls_post_creation_hooks(self, mock_async_post_create_hook, mock_sync_post_create_hooks):
        user = get_user_model().objects.create(username="user")
        self.client.force_login(user)

        with open(".test/images/palpatine.jpg", "rb") as f:
            request_data = {
                "name": "upload test",
                "labels": [],
                "image": f,
                "visibility": 3,
            }
            response = self.client.post("/api/photos/", data=request_data)
            data = json.loads(response.content)
            photo = Photo.objects.get(pk=data["id"])

            mock_async_post_create_hook.assert_called_once_with(photo)
            mock_sync_post_create_hooks.assert_called_once_with(photo)

    @patch("photo.locations.tasks.photo_post_create_async")
    @patch("photo.locations.service.weather._fetch_api_data")
    def testCreation_weatherAtCreation(self, mock_fetch_api_data, mock_async_post_create_hook):
        mock_fetch_api_data.return_value = {
            "weather": [{"id": 800, "main": "Clear", "description": "clear sky", "icon": "01d"}],
            "main": {
                "temp": 42.11,
            },
            "sys": {"sunrise": 1560343627, "sunset": 1560396563},
            "id": 420006353,
            "name": "Mountain View",
        }

        user = get_user_model().objects.create(username="user")
        self.client.force_login(user)

        with open(".test/images/palpatine.jpg", "rb") as f:
            request_data = {
                "name": "upload test",
                "description": "It’s the ship that made the Kessel run in less than twelve parsecs.",
                "geo": '{"type": "Point","coordinates": [13.0468,47.801186]}',
                "labels": [],
                "image": f,
                "visibility": 3,
            }
            response = self.client.post("/api/photos/", data=request_data)
            data = json.loads(response.content)
            photo = Photo.objects.get(pk=data["id"])

            mock_fetch_api_data.assert_called_with(13.0468, 47.801186)
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            self.assertIsInstance(photo.weatherAtCreation, WeatherState)
            self.assertEqual(photo.weatherAtCreation.temperature, 42.11)

    def testPhotoViewSet_permissions_unauthenticated__read(self):
        user = get_user_model().objects.create(username="user")
        photo = Photo.objects.create(user=user, name="test image", visibility=Photo.Visibility.PUBLIC)

        r = self.client.get(f"/api/photos/{photo.id}/")

        self.assertEqual(
            r.status_code, status.HTTP_200_OK, "an unauthenticated user should be able to read public photos"
        )

    def testPhotoViewSet_permissions_unauthenticated__post(self):
        r = self.client.post(
            "/api/photos/",
            data={
                "name": "upload test",
                "visibility": Photo.Visibility.PUBLIC,
            },
            content_type="application/json",
        )

        self.assertEqual(
            r.status_code, status.HTTP_401_UNAUTHORIZED, "an unauthenticated user should not be able to create a photo"
        )

    def testPhotoViewSet_permissions_unauthenticated__patch(self):
        user = get_user_model().objects.create(username="user")
        photo = Photo.objects.create(user=user, name="test image", visibility=Photo.Visibility.PUBLIC)

        r = self.client.patch(
            f"/api/photos/{photo.id}/",
            data={
                "name": "upload test",
                "visibility": Photo.Visibility.PUBLIC,
            },
            content_type="application/json",
        )

        self.assertEqual(
            r.status_code, status.HTTP_401_UNAUTHORIZED, "an unauthenticated user should not be able to patch a photo"
        )

    def testPhotoViewSet_permissions_unauthenticated__put(self):
        user = get_user_model().objects.create(username="user")
        photo = Photo.objects.create(user=user, name="test image", visibility=Photo.Visibility.PUBLIC)

        r = self.client.put(f"/api/photos/{photo.id}/", data={}, content_type="application/json")

        self.assertEqual(
            r.status_code, status.HTTP_401_UNAUTHORIZED, "an unauthorized user should not be able to update a photo"
        )

    def testPhotoViewSet_permissions_unauthenticated__delete(self):
        user = get_user_model().objects.create(username="user")
        photo = Photo.objects.create(user=user, name="test image", visibility=Photo.Visibility.PUBLIC)

        r = self.client.delete(f"/api/photos/{photo.id}/")

        self.assertEqual(
            r.status_code, status.HTTP_401_UNAUTHORIZED, "an unauthorized user should not be able to delete a photo"
        )

    @patch("photo.locations.tasks.photo_post_create_async")
    @patch("photo.locations.tasks.photo_post_update_async")
    def testPhotoViewSet_permissions__read(self, post_update_hook, post_create__hook):
        user = get_user_model().objects.create(username="user")
        photo = Photo.objects.create(user=user, name="test image", visibility=Photo.Visibility.PUBLIC)

        self.client.force_login(user)
        r = self.client.get(f"/api/photos/{photo.id}/")

        self.assertEqual(r.status_code, status.HTTP_200_OK, "a user should be able to read his own photos")

    @patch("photo.locations.tasks.photo_post_create_async")
    def testPhotoViewSet_permissions__create(self, post_create__hook):
        user = get_user_model().objects.create(username="user")

        self.client.force_login(user)
        r = self.client.post(
            "/api/photos/",
            data={
                "name": "upload test",
                "visibility": Photo.Visibility.PUBLIC,
            },
            content_type="application/json",
        )

        self.assertEqual(r.status_code, status.HTTP_201_CREATED, "a user should be able to create a photo")

    @patch("photo.locations.tasks.photo_post_update_async")
    def testPhotoViewSet_permissions__patch(self, post_update_hook):
        user = get_user_model().objects.create(username="user")
        photo = Photo.objects.create(user=user, name="test image", visibility=Photo.Visibility.PUBLIC)

        self.client.force_login(user)
        r = self.client.patch(
            f"/api/photos/{photo.id}/",
            data={
                "name": "upload test",
                "visibility": Photo.Visibility.PUBLIC,
            },
            content_type="application/json",
        )

        self.assertEqual(r.status_code, status.HTTP_200_OK, "a user should be able to update his own photo")

    @patch("photo.locations.tasks.photo_post_update_async")
    def testPhotoViewSet_permissions__update(self, post_update_hook):
        user = get_user_model().objects.create(username="user")
        photo = Photo.objects.create(user=user, name="test image", visibility=Photo.Visibility.PUBLIC)

        self.client.force_login(user)
        r = self.client.put(f"/api/photos/{photo.id}/", data={}, content_type="application/json")

        self.assertEqual(r.status_code, status.HTTP_200_OK, "a user should be able to update his own photo")

    def testPhotoViewSet_permissions__delete(self):
        user = get_user_model().objects.create(username="user")
        photo = Photo.objects.create(user=user, name="test image", visibility=Photo.Visibility.PUBLIC)

        self.client.force_login(user)
        r = self.client.delete(f"/api/photos/{photo.id}/")

        self.assertEqual(r.status_code, status.HTTP_204_NO_CONTENT, "a user should be able to delete his own photo")

    def testPhotoViewSet_permissions_another_users_photo__read(self):
        user = get_user_model().objects.create(username="user")
        another_user = get_user_model().objects.create(username="another_user", email="test@test.de")
        another_users_photo = Photo.objects.create(
            user=another_user, name="test image", visibility=Photo.Visibility.PUBLIC
        )

        self.client.force_login(user)
        r = self.client.get(f"/api/photos/{another_users_photo.id}/")

        self.assertEqual(r.status_code, status.HTTP_200_OK, "should be able to read another user's public photo")

    def testPhotoViewSet_permissions_another_users_photo__patch(self):
        user = get_user_model().objects.create(username="user")
        another_user = get_user_model().objects.create(username="another_user", email="test@test.de")
        another_users_photo = Photo.objects.create(
            user=another_user, name="test image", visibility=Photo.Visibility.PUBLIC
        )

        self.client.force_login(user)
        r = self.client.patch(
            f"/api/photos/{another_users_photo.id}/",
            data={
                "name": "upload test",
                "visibility": Photo.Visibility.PUBLIC,
            },
            content_type="application/json",
        )

        self.assertEqual(r.status_code, status.HTTP_403_FORBIDDEN, "should not be able to patch another user's photo")

    def testPhotoViewSet_permissions_another_users_photo__update(self):
        user = get_user_model().objects.create(username="user")
        another_user = get_user_model().objects.create(username="another_user", email="test@test.de")
        photo = Photo.objects.create(user=another_user, name="test image", visibility=Photo.Visibility.PUBLIC)

        self.client.force_login(user)
        r = self.client.put(f"/api/photos/{photo.id}/", data={}, content_type="application/json")

        self.assertEqual(r.status_code, status.HTTP_403_FORBIDDEN, "should not be able to update another user's photo")

    def testPhotoViewSet_permissions_another_users_photo__delete(self):
        user = get_user_model().objects.create(username="user")
        another_user = get_user_model().objects.create(username="another_user", email="test@test.de")
        photo = Photo.objects.create(user=another_user, name="test image", visibility=Photo.Visibility.PUBLIC)

        self.client.force_login(user)
        r = self.client.delete(f"/api/photos/{photo.id}/")

        self.assertEqual(r.status_code, status.HTTP_403_FORBIDDEN, "should not be able to delete another users photo")

    @patch("photo.locations.service.photo.requests.get", return_value=_get_solar_data_mock(True))
    def testPhotoSolarData(self, request_get):
        user = get_user_model().objects.create(
            email="test@test.de",
            username="foobar",
        )
        photo = Photo.objects.create(
            name="image-1",
            visibility=Photo.Visibility.PUBLIC,
            geo=Point(x=40, y=10),
            originalCreationDate="2020-09-21T18:13:49Z",
            guid="1",
            user=user,
        )

        response = self.client.get(f"/api/photos/{photo.id}/solar_data/")
        solar_data = SolarData.objects.filter(photo=photo, date="2020-09-21").first()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsInstance(solar_data, SolarData)
        self.assertEqual(solar_data.sunrise, datetime.fromisoformat("2015-05-21T05:05:35+00:00"))
        self.assertEqual(solar_data.sunset, datetime.fromisoformat("2015-05-21T19:22:59+00:00"))
        self.assertEqual(solar_data.dayLength, 51444)

    @patch("photo.locations.service.photo.requests.get", return_value=_get_solar_data_mock(True))
    def testPhotoSolarData__caches_results(self, request_get):
        user = get_user_model().objects.create(
            email="test@test.de",
            username="foobar",
        )
        photo = Photo.objects.create(
            name="image-1",
            visibility=Photo.Visibility.PUBLIC,
            geo=Point(x=40, y=10),
            originalCreationDate="2020-09-21T18:13:49Z",
            guid="1",
            user=user,
        )

        self.client.get(f"/api/photos/{photo.id}/solar_data/")
        response = self.client.get(f"/api/photos/{photo.id}/solar_data/")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        request_get.assert_called_once()

    @patch("photo.locations.service.photo.requests.get", return_value=_get_solar_data_mock(True))
    def testPhotoSolarData__date_parameter(self, request_get):
        user = get_user_model().objects.create(
            email="test@test.de",
            username="foobar",
        )
        photo = Photo.objects.create(
            name="image-1",
            visibility=Photo.Visibility.PUBLIC,
            geo=Point(x=40, y=10),
            originalCreationDate="2020-09-21T18:13:49Z",
            guid="1",
            user=user,
        )

        response = self.client.get(f"/api/photos/{photo.id}/solar_data/?date=2020-01-01")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        request_get.assert_called_once()
        self.assertEqual(len(SolarData.objects.filter(photo=photo, date="2020-01-01")), 1)

    @patch("photo.locations.service.photo.requests.get", return_value=_get_solar_data_mock(False))
    def testPhotoSolarData__invalid_date(self, request_get):
        user = get_user_model().objects.create(
            email="test@test.de",
            username="foobar",
        )
        photo = Photo.objects.create(
            name="image-1",
            visibility=Photo.Visibility.PUBLIC,
            geo=Point(x=40, y=10),
            originalCreationDate="2020-09-21T18:13:49Z",
            guid="1",
            user=user,
        )

        response = self.client.get(f"/api/photos/{photo.id}/solar_data/?date=nonsense")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(SolarData.objects.filter(photo=photo)), 0)
        request_get.assert_not_called()

    def test_photo_data__get(self):
        photo = Photo.objects.create(name="a simple photo", visibility=Photo.Visibility.PUBLIC, guid=1)
        photo_data = PhotoData.objects.create(photo=photo)

        response = self.client.get(f"/api/photos/{photo.id}/photo_data/")
        self.assertEqual(
            response.status_code, status.HTTP_200_OK, "should successfully get the photoData object of a photo"
        )

        data = json.loads(response.content)
        self.assertIn("id", data, "response should include an id field of the photoData object")
        self.assertEqual(data["id"], photo_data.id, "response should include the id of the requested photoData object")

    def test_photo_data__get__missing_photo(self):
        id_of_missing_photo = 42
        response = self.client.get(f"/api/photos/{id_of_missing_photo}/photo_data/")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND, "should return 404 if photo was not found")

    @patch("photo.locations.tasks.photo_post_update_async")
    def test_update__creates_not_existing_labels(self, _):
        title_of_label = "a new label"
        user = get_user_model().objects.create()
        photo = Photo.objects.create(user=user)
        self.client.force_login(user)

        self.client.put(
            f"/api/photos/{photo.id}/",
            data={"labels": [{"title": title_of_label, "score": 1.0}]},
            content_type="application/json",
        )

        label = Label.objects.filter(title=title_of_label).first()
        self.assertIsInstance(label, Label, "should create a new label if it does not exist")
        self.assertEqual(photo.labels.first().id, label.id, "should assign the freshly created label to the photo")

    @patch("photo.locations.tasks.photo_post_update_async")
    def test_update__assigns_existing_labels(self, _):
        user = get_user_model().objects.create()
        photo = Photo.objects.create(user=user)
        Label.objects.bulk_create([Label(title="A"), Label(title="B"), Label(title="C")])
        self.client.force_login(user)

        self.client.put(
            f"/api/photos/{photo.id}/",
            data={"labels": [{"title": "A"}, {"title": "B"}, {"title": "C"}]},
            content_type="application/json",
        )

        self.assertEqual(photo.labels.count(), 3, "should assign the 3 labels to the photo")
        self.assertIn(photo.labels.first().title, ["A", "B", "C"], "should assign the correct labels")

    @patch("photo.locations.service.photo.get_visually_similar_photos")
    def test_similar(self, get_visually_similar_photos):
        photo = Photo.objects.create(name="a simple photo", visibility=Photo.Visibility.PUBLIC)
        photo_similar_1 = Photo.objects.create(name="a similar photo 1", visibility=Photo.Visibility.PUBLIC)
        photo_similar_2 = Photo.objects.create(name="a similar photo 2", visibility=Photo.Visibility.PUBLIC)

        mock_return_value = (
            photo_similar_1,
            photo_similar_2,
        )
        get_visually_similar_photos.return_value = mock_return_value

        response = self.client.get(f"/api/photos/{photo.id}/similar/")
        self.assertEqual(response.status_code, status.HTTP_200_OK, "should successfully load similar photos")
        data = json.loads(response.content)
        self.assertEqual(
            len(data), len(mock_return_value), "should return 2 similar photos retrieved from imgsim service"
        )

    def test_map__global_view(self):
        _photo_map_fixtures()

        lat1, lon1, lat2, lon2, zoom = 65.73, -151.17, 5.26, 184.21, 3
        response = self.client.get(f"/api/photos/map/?lat1={lat1}&long1={lon1}&lat2={lat2}&long2={lon2}&zoom={zoom}")

        self.assertEqual(response.status_code, status.HTTP_200_OK, "should load clustered photos")
        self.assertEqual(
            len(json.loads(response.content)), 2, "global view should return two main clusters (salzburg and vienna)"
        )

    def test_map__austria_view(self):
        _photo_map_fixtures()

        lat1, lon1, lat2, lon2, zoom = 48.89, 9.72, 47.20, 20.20, 8
        response = self.client.get(f"/api/photos/map/?lat1={lat1}&long1={lon1}&lat2={lat2}&long2={lon2}&zoom={zoom}")

        self.assertEqual(response.status_code, status.HTTP_200_OK, "should load clustered photos")
        self.assertEqual(
            len(json.loads(response.content)),
            3,
            "austria centered view should return three main clusters (salzburg, fuschl and vienna)",
        )

    def test_map__salzburg_view(self):
        _photo_map_fixtures()

        lat1, lon1, lat2, lon2, zoom = 47.81, 12.97, 47.783, 13.13, 14
        response = self.client.get(f"/api/photos/map/?lat1={lat1}&long1={lon1}&lat2={lat2}&long2={lon2}&zoom={zoom}")

        self.assertEqual(response.status_code, status.HTTP_200_OK, "should load clustered photos")
        data = json.loads(response.content)
        self.assertEqual(len(data), 2, "salzburg centered view should return two main clusters")
        self.assertIn("salzburg", data[0]["photos"][0]["name"], "the clustered photo should be located in salzburg")
        self.assertIn("salzburg", data[1]["photos"][0]["name"], "the clustered photo should be located in salzburg")


@patch("photo.api.views.photo.photo_search")
class TestPhotoSearchTest(TestCase):
    def test_search__no_query_parameter(self, els_search):
        els_search.to_queryset.return_value = []

        response = self.client.get("/api/photos/search/")
        self.assertEqual(response.status_code, status.HTTP_200_OK, "successfully loads a list of photos")

    def test_search__query_parameter(self, els_search):
        els_search.to_queryset.return_value = []

        response = self.client.get("/api/photos/search/?q=salzburg")
        _, kwargs = els_search.call_args_list[0]

        self.assertEqual(response.status_code, status.HTTP_200_OK, "successfully loads a list of photos")
        self.assertTrue("q" in kwargs, "Slug not passed to elastic search search function")
        self.assertEqual(kwargs["q"], "salzburg")

    def test_search__query_parameter_white_space(self, els_search):
        els_search.to_queryset.return_value = []

        response = self.client.get("/api/photos/search/?q=salzburg+land")
        _, kwargs = els_search.call_args_list[0]

        self.assertEqual(response.status_code, status.HTTP_200_OK, "successfully loads a list of photos")
        self.assertTrue("q" in kwargs, "Slug not passed to elastic search search function")
        self.assertEqual(kwargs["q"], "salzburg land")

    def test_search__additional_parameter(self, els_search):
        els_search.to_queryset.return_value = []

        response = self.client.get("/api/photos/search/?weather=sunny&season=winter&make=Apple&model=iPhone")
        _, kwargs = els_search.call_args_list[0]

        self.assertEqual(response.status_code, status.HTTP_200_OK, "successfully loads a list of photos")

        for key in ["weather", "season", "make", "model"]:
            with self.subTest(key):
                self.assertTrue(key in kwargs, "Slug " + key + " not passed to elastic search search function")
