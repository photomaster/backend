from django.contrib.auth import get_user_model
from django.test import TestCase
from rest_framework import status

from photo.locations.models import WeatherState


class WeatherStateViewSetTest(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create(username="user", displayName="Otto Normal", email="test@test.de")
        self.another_user = get_user_model().objects.create(
            username="another_user", displayName="Otto Normal", email="test@test1.de"
        )

    def testUpdate__user_is_owner__patch(self):
        weather_state = WeatherState.objects.create(temperature=10.0, description="sunny", user=self.user)
        self.client.force_login(self.user)

        response = self.client.patch(
            "/api/weatherState/{}/".format(weather_state.id),
            data={"temperature": 15, "description": "cloudy"},
            content_type="application/json",
        )

        self.assertEqual(
            response.status_code, status.HTTP_200_OK, "a user should be able to update his own weather states"
        )

    def testUpdate__user_is_owner__put(self):
        weather_state = WeatherState.objects.create(temperature=10.0, description="sunny", user=self.user)
        self.client.force_login(self.user)

        response = self.client.put(
            "/api/weatherState/{}/".format(weather_state.id),
            data={"temperature": 15, "description": "cloudy"},
            content_type="application/json",
        )
        self.assertEqual(
            response.status_code, status.HTTP_200_OK, "a user should be able to update his own weather states"
        )

    def testUpdate__not_authenticated__patch(self):
        weather_state = WeatherState.objects.create(temperature=10.0, description="sunny", user=self.user)
        response = self.client.patch(
            "/api/weatherState/{}/".format(weather_state.id),
            data={"temperature": 15, "description": "cloudy"},
            content_type="application/json",
        )
        self.assertEqual(
            response.status_code,
            status.HTTP_401_UNAUTHORIZED,
            "unauthenticated sessions should not be able to update weather states",
        )

    def testUpdate__not_authenticated__put(self):
        weather_state = WeatherState.objects.create(temperature=10.0, description="sunny", user=self.user)

        response = self.client.put(
            "/api/weatherState/{}/".format(weather_state.id),
            data={"temperature": 15, "description": "cloudy"},
            content_type="application/json",
        )
        self.assertEqual(
            response.status_code,
            status.HTTP_401_UNAUTHORIZED,
            "unauthenticated sessions should not be able to update weather states",
        )

    def testUpdate__not_owner__patch(self):
        weather_state = WeatherState.objects.create(temperature=10.0, description="sunny", user=self.user)
        self.client.force_login(self.another_user)

        response = self.client.patch(
            "/api/weatherState/{}/".format(weather_state.id),
            data={"temperature": 15, "description": "cloudy"},
            content_type="application/json",
        )
        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN,
            "a user should not be able to update another users weather states",
        )

    def testUpdate__not_owner__put(self):
        weather_state = WeatherState.objects.create(temperature=10.0, description="sunny", user=self.user)
        self.client.force_login(self.another_user)

        response = self.client.put(
            "/api/weatherState/{}/".format(weather_state.id),
            data={"temperature": 15, "description": "cloudy"},
            content_type="application/json",
        )

        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN,
            "a user should not be able to update another users weather states",
        )

    def testUpdate__not_owned_states__patch(self):
        weather_state = WeatherState.objects.create(temperature=10.0, description="sunny")
        self.client.force_login(self.user)

        response = self.client.patch(
            "/api/weatherState/{}/".format(weather_state.id),
            data={"temperature": 15, "description": "cloudy"},
            content_type="application/json",
        )

        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN,
            "weather states without an assigned user should never be editable. these are shared weather objects",
        )

    def testUpdate__not_owned_states(self):
        weather_state = WeatherState.objects.create(temperature=10.0, description="sunny")
        self.client.force_login(self.user)

        response = self.client.put(
            "/api/weatherState/{}/".format(weather_state.id),
            data={"temperature": 15, "description": "cloudy"},
            content_type="application/json",
        )
        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN,
            "weather states without an assigned user should never be editable. these are shared weather objects",
        )
