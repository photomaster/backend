import io
import json
from unittest import mock
from unittest.mock import Mock

from PIL import Image
from google.cloud import vision_v1p4beta1


def _generate_photo_file(filename="test"):
    file = io.BytesIO()
    image = Image.new("RGBA", size=(5, 5), color=(155, 0, 0))
    image.save(file, "png")
    file.name = filename + ".png"
    file.seek(0)
    return file


def _vision_api_mock_response(safe_search_key):
    from google.cloud.vision_v1.proto import image_annotator_pb2
    from photo.locations.service.photo import ChannelStub

    with open(".test/google_vision_response.json") as mock_response:
        response_data = json.loads(mock_response.read())
    response_data["safe_search_annotation"][safe_search_key] = "POSSIBLE"

    expected_response = image_annotator_pb2.BatchAnnotateImagesResponse(**{"responses": [response_data]})
    # Mock the API response
    channel = ChannelStub(responses=[expected_response])
    patch = mock.patch("google.api_core.grpc_helpers.create_channel")
    with patch as create_channel:
        create_channel.return_value = channel
        client = vision_v1p4beta1.ImageAnnotatorClient()
    return client.batch_annotate_images([]).responses[0]


def _get_solar_data_mock(is_request_successful: bool):
    mock_response = Mock()
    mock_response.ok = is_request_successful
    if is_request_successful:
        mock_response.json.return_value = {
            "results": {
                "sunrise": "2015-05-21T05:05:35+00:00",
                "sunset": "2015-05-21T19:22:59+00:00",
                "solar_noon": "2015-05-21T12:14:17+00:00",
                "day_length": 51444,
                "civil_twilight_begin": "2015-05-21T04:36:17+00:00",
                "civil_twilight_end": "2015-05-21T19:52:17+00:00",
                "nautical_twilight_begin": "2015-05-21T04:00:13+00:00",
                "nautical_twilight_end": "2015-05-21T20:28:21+00:00",
                "astronomical_twilight_begin": "2015-05-21T03:20:49+00:00",
                "astronomical_twilight_end": "2015-05-21T21:07:45+00:00",
            }
        }
    return mock_response


def _photo_map_fixtures():
    from photo.locations.service.map import generate_detail_clusters

    from photo.locations.models import Photo
    from django.contrib.gis.geos import Point

    Photo.objects.create(
        name="salzburg", visibility=Photo.Visibility.PUBLIC, geo=Point(13.043244468914429, 47.79935700041912)
    ),
    Photo.objects.create(
        name="near salzburg", visibility=Photo.Visibility.PUBLIC, geo=Point(13.045647728265912, 47.80310443720898)
    )
    Photo.objects.create(name="vienna", visibility=Photo.Visibility.PUBLIC, geo=Point(16.440, 48.16))
    Photo.objects.create(name="vienna", visibility=Photo.Visibility.PUBLIC, geo=Point(16.52, 48.13))
    Photo.objects.create(name="fuschl", visibility=Photo.Visibility.PUBLIC, geo=Point(13.295, 47.791))

    generate_detail_clusters()
