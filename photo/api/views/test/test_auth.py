import json
from unittest.mock import patch

from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.test import TestCase
from rest_framework import status


class AuthViewTest(TestCase):
    def setUp(self):
        self.password = "1234"

        user = get_user_model().objects.create(username="user1", displayName="Otto Normal", email="test@test.de")
        user.set_password(self.password)
        user.save()

        self.user = user

    def test_register_username_taken(self):
        response = self.client.post(
            "/api/auth/register/", data={"username": self.user.username, "password": "0000", "email": "foo@bar.at"}
        )
        self.assertEqual(
            response.status_code,
            status.HTTP_400_BAD_REQUEST,
            "should not create account since username is already taken",
        )

    def test_register_email_taken(self):
        response = self.client.post(
            "/api/auth/register/", data={"username": "user2", "password": "0000", "email": self.user.email}
        )
        self.assertEqual(
            response.status_code, status.HTTP_400_BAD_REQUEST, "should not create account since email is already taken"
        )

    def test_register_username_missing(self):
        response = self.client.post("/api/auth/register/", data={"email": "foo@bar.at", "password": "1234"})
        self.assertEqual(
            response.status_code, status.HTTP_400_BAD_REQUEST, "should not create account since username is missing"
        )

    def test_register_password_missing(self):
        response = self.client.post("/api/auth/register/", data={"username": "user2", "email": "foo@bar.at"})
        self.assertEqual(
            response.status_code, status.HTTP_400_BAD_REQUEST, "should not create account since password is missing"
        )

    def test_register_all_params_missing(self):
        response = self.client.post("/api/auth/register/")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, "expect 400 since parameters are missing")

    @patch("django_pwned_passwords.password_validation.PWNEDPasswordValidator.check_valid", return_value=True)
    def test_register(self, _):
        response = self.client.post(
            "/api/auth/register/",
            data={
                "username": "foobar123",
                "displayName": "Foo Bar",
                "password": "0000",
                "email": "foo@bar.at",
            },
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, "should successfully create new account")
        data = json.loads(response.content)
        self.assertEqual(data["username"], "foobar123")
        self.assertEqual(data["displayName"], "Foo Bar")
        self.assertEqual(data["email"], "foo@bar.at")
        self.assertFalse("password" in data, "should not expose password!")

    @patch(
        "django_pwned_passwords.password_validation.PWNEDPasswordValidator.check_valid",
        side_effect=ValidationError("breached password"),
    )
    def test_register__weak_password(self, password_validator):
        response = self.client.post(
            "/api/auth/register/",
            data={
                "username": "foobar123",
                "displayName": "Foo Bar",
                "password": "0000",
                "email": "foo@bar.at",
            },
        )
        self.assertEqual(
            response.status_code,
            status.HTTP_400_BAD_REQUEST,
            "should prevent the creation of users using a breached password",
        )
        password_validator.assert_called()

    def test_login_missing_parameters(self):
        response = self.client.post("/api/auth/")
        self.assertEqual(
            response.status_code, status.HTTP_400_BAD_REQUEST, "expect 400 since all parameters are missing"
        )

        response = self.client.post("/api/auth/", data={"username": "", "password": ""})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        response = self.client.post("/api/auth/", data={"username": self.user.username})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_login_wrong_password(self):
        response = self.client.post("/api/auth/", data={"username": self.user.username, "password": "foobar"})
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_login(self):
        response = self.client.post("/api/auth/", data={"username": self.user.username, "password": self.password})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = json.loads(response.content)
        self.assertTrue("access" in data, "response should include token")
        self.assertTrue("refresh" in data, "response should include refresh token")

    def test_login_with_email_wrong_password(self):
        response = self.client.post("/api/auth/", data={"username": self.user.email, "password": "wrong_password"})
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_login_with_email(self):
        response = self.client.post("/api/auth/", data={"username": self.user.email, "password": self.password})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = json.loads(response.content)
        self.assertTrue("access" in data, "response should include token")
        self.assertTrue("refresh" in data, "response should include refresh token")
