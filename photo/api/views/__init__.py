from .favorites import FavoritePhotosViewSet
from .locations import LocationViewSet
from .other import AuthView, UserViewSet, GroupViewSet, PhotoDataViewSet, WeatherStateViewSet, LabelViewSet
from .photo import PhotoViewSet
from .profile import ProfileViewSet
