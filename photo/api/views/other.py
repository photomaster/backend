from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from rest_framework import viewsets, permissions
from rest_framework.decorators import action
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response

from photo.api.views.photo import filter_photo_queryset
from photo.locations import tasks
from photo.locations.models import Photo, WeatherState, Label
from photo.locations.models import PhotoData
from ..permissions import IsOwnerOrIsReadOnly
from ..serializers import (
    UserSerializer,
    GroupSerializer,
    PhotoSerializer,
    WeatherStateSerializer,
    LabelSerializer,
    AuthenticatedUserSerializer,
    PhotoDataSerializer,
)


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """

    queryset = get_user_model().objects.all().order_by("-date_joined")
    serializer_class = UserSerializer

    @action(detail=True, methods=("GET",))
    def photos(self, request, pk):
        user = self.get_object()
        query_set = Photo.objects.filter(user=user, visibility=Photo.Visibility.PUBLIC)
        query_set = filter_photo_queryset(query_set, request)
        serializer = PhotoSerializer(query_set, many=True, context=self.get_serializer_context())
        return Response(serializer.data)

    def partial_update(self, request, *args, **kwargs):
        kwargs["partial"] = True
        return self.update(request, *args, **kwargs)

    def get_serializer(self, *args, **kwargs):
        if "instance" not in kwargs and len(args) == 0:
            return super(UserViewSet, self).get_serializer(*args, **kwargs)

        serializer_class = self.get_serializer_class()
        instance = kwargs["instance"] if "instance" in kwargs and kwargs["instance"] else args[0]
        if instance == self.request.user:
            serializer_class = AuthenticatedUserSerializer
        kwargs["context"] = self.get_serializer_context()
        return serializer_class(*args, **kwargs)


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """

    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class PhotoDataViewSet(viewsets.ModelViewSet):
    permission_classes = (IsOwnerOrIsReadOnly,)
    queryset = PhotoData.objects.all()
    serializer_class = PhotoDataSerializer

    def perform_update(self, serializer):
        instance = serializer.save()

        tasks.photodata_post_update_async(instance)


class WeatherStateViewSet(viewsets.ModelViewSet):
    permission_classes = (IsOwnerOrIsReadOnly,)
    queryset = WeatherState.objects.all()
    serializer_class = WeatherStateSerializer


class LabelViewSet(viewsets.ModelViewSet):
    queryset = Label.objects.all()
    serializer_class = LabelSerializer


class AuthView(CreateAPIView):
    model = get_user_model()
    permission_classes = (permissions.AllowAny,)
    serializer_class = AuthenticatedUserSerializer
