from django.contrib.auth import get_user_model
from rest_framework import mixins, permissions
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from photo.api.serializers import AuthenticatedUserSerializer, PhotoSerializer
from photo.locations.models import Photo


class ProfileViewSet(mixins.RetrieveModelMixin, mixins.UpdateModelMixin, GenericViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = AuthenticatedUserSerializer
    queryset = get_user_model().objects.all()

    def list(self, request):
        return Response(self.serializer_class(request.user, context={"request": request}).data)

    def put(self, request, *args, **kwargs):
        return super(ProfileViewSet, self).update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return super(ProfileViewSet, self).partial_update(request, *args, **kwargs)

    @action(methods=("GET",), detail=False)
    def photos(self, request):
        photos = (
            Photo.objects.filter(user=request.user)
            .select_related("user", "photodata")
            .prefetch_related("label_photos__label")
        )

        page = self.paginate_queryset(photos)
        if page is not None:
            serializer = PhotoSerializer(page, many=True, context={"request": request})
            return self.get_paginated_response(serializer.data)

        return Response(PhotoSerializer(photos, many=True, context={"request": request}).data)

    def get_object(self):
        return self.request.user
