from django.http import Http404
from rest_framework import viewsets, permissions, status
from rest_framework.response import Response

from photo.api.permissions import IsOwner
from photo.api.serializers import FavoritePhotoSerializer
from photo.locations.models import Photo, FavoritePhoto


class FavoritePhotosViewSet(viewsets.ModelViewSet):
    serializer_class = FavoritePhotoSerializer
    permission_classes = (
        permissions.IsAuthenticated,
        IsOwner,
    )

    def create(self, request, *args, **kwargs):
        if (
            not request.data
            or "photo" not in request.data
            or "id" not in request.data["photo"]
            or not isinstance(request.data["photo"]["id"], int)
        ):
            return Response(status=status.HTTP_400_BAD_REQUEST)

        response_status = status.HTTP_200_OK

        favorite = self.get_queryset().filter(user=request.user, photo_id=request.data["photo"]["id"]).first()
        if not favorite:
            photo = Photo.objects.filter(pk=request.data["photo"]["id"]).first()
            if not photo:
                raise Http404("photo not found")

            response_status = status.HTTP_201_CREATED
            favorite = FavoritePhoto(user=request.user, photo=photo)
            favorite.save()

        return Response(
            FavoritePhotoSerializer(favorite, many=False, context={"request": request}).data, status=response_status
        )

    def get_queryset(self):
        return FavoritePhoto.objects.filter(user=self.request.user).prefetch_related("photo__label_photos__label")
