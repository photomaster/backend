from datetime import datetime

from django.contrib.gis.db.models.functions import GeometryDistance
from django.contrib.gis.geos import Point
from django.http import Http404
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.exceptions import ParseError, PermissionDenied
from rest_framework.response import Response

from photo.api.filters import PhotoFilterBackend, LabelsFilterBackend
from photo.api.permissions import IsOwnerOrIsReadOnly
from photo.api.serializers import PhotoSerializer, PhotoDataSerializer, SolarDataSerializer, ClusterSerializer
from photo.locations import tasks
from photo.locations.elastic.autocomplete import AutoCompletion
from photo.locations.elastic.search import photo_search
from photo.locations.models import Photo
from photo.locations.service import map as map_service, photo as photo_service


def cast_parameter_to_int(key, request, fallback=0, max_v=None, min_v=None):
    try:
        v = request.query_params.get(key, fallback)
        if not isinstance(v, int):
            v = int(float(v))
        if max_v is not None and v > max_v:
            return max_v
        elif min_v is not None and v < min_v:
            return min_v
        return v
    except BaseException:
        return fallback


def naive_km_to_degrees(km):
    return km / 40075 * 360


def filter_photo_queryset(query_set, request):
    if "lat" in request.query_params and "long" in request.query_params:
        lat = float(request.query_params["lat"])
        long = float(request.query_params["long"])
        distance = float(request.query_params["distance"]) if "distance" in request.query_params else 25

        geo_point = Point(long, lat)
        query_set = (
            query_set.filter(geo__dwithin=(geo_point, naive_km_to_degrees(distance)))
            .annotate(distance=GeometryDistance("geo", geo_point))
            .order_by("distance")
        )

    return query_set


class PhotoViewSet(viewsets.ModelViewSet):
    permission_classes = (IsOwnerOrIsReadOnly,)
    serializer_class = PhotoSerializer
    queryset = Photo.objects.select_related("user", "photodata").prefetch_related("label_photos__label")

    filter_backends = [PhotoFilterBackend, LabelsFilterBackend]
    ordering_fields = ["id", "name", "originalCreationDate", "popularity"]

    def filter_queryset(self, queryset):
        queryset = super(PhotoViewSet, self).filter_queryset(queryset)
        if self.request.user and self.request.user.is_authenticated:
            queryset = queryset.filter(visibility=Photo.Visibility.PUBLIC) | queryset.filter(user=self.request.user)
        else:
            queryset = queryset.filter(visibility=Photo.Visibility.PUBLIC)
        return filter_photo_queryset(queryset, self.request)

    def perform_update(self, serializer):
        instance = serializer.save()

        tasks.photo_post_update_sync(instance)
        tasks.photo_post_update_async(instance)

    def partial_update(self, request, *args, **kwargs):
        kwargs["partial"] = True
        return self.update(request, *args, **kwargs)

    def perform_create(self, serializer):
        instance = (
            serializer.save() if not self.request.user.is_authenticated else serializer.save(user=self.request.user)
        )

        tasks.photo_post_create_sync(instance)
        tasks.photo_post_create_async(instance)

    @action(detail=True, methods=["get"])
    def photo_data(self, request, pk):
        try:
            photo = Photo.objects.get(pk=pk)
            return Response(PhotoDataSerializer(photo.photodata, many=False, context={"request": request}).data)
        except BaseException:
            raise Http404()

    @action(detail=True, methods=["get"])
    def solar_data(self, request, pk):
        if "date" not in request.query_params or request.query_params["date"] == "null":
            photo = Photo.objects.get(pk=pk)
            date = photo.originalCreationDate.strftime("%Y-%m-%d")
        else:
            try:
                date = datetime.strptime(request.query_params["date"], "%Y-%m-%d").strftime("%Y-%m-%d")
            except ValueError:
                raise ParseError(detail="malformed date format. Expected format 'Y-m-d'")

        solar_data = photo_service.get_solar_data(pk, date)
        if not solar_data:
            raise Http404()

        serializer = SolarDataSerializer(solar_data, many=False, context={"request": request})
        return Response(serializer.data)

    @action(detail=True, methods=["get"])
    def similar(self, request, pk=None):
        photo = self.get_object()
        similar_photos = photo_service.get_similiar_photos(photo)
        serializer = self.serializer_class(similar_photos, many=True, context={"request": request})

        return Response(serializer.data)

    @action(detail=False, methods=("get",))
    def map(self, request):
        for param in ("lat1", "long1", "lat2", "long2"):
            if param not in request.query_params:
                return Response(status=status.HTTP_400_BAD_REQUEST)

        photos = map_service.get_entries(
            request.query_params["lat1"],
            request.query_params["long1"],
            request.query_params["lat2"],
            request.query_params["long2"],
            cast_parameter_to_int("zoom", request, 9, 18, 3),
        )
        serializer = ClusterSerializer(photos, many=True, context={"request": request})
        return Response(serializer.data)

    @action(detail=False, methods=("GET",))
    def search(self, request):
        offset = int(request.query_params.get("offset", 0))
        page_size = 9

        search_args = {
            **{k: v for k, v in request.query_params.items()},
            "q": request.query_params.get("q"),
            "weather": request.query_params.get("weather"),
            "season": request.query_params.get("season"),
        }

        qs = photo_search(**search_args)[offset : offset + page_size].to_queryset()

        return Response(PhotoSerializer(qs, many=True, context={"request": request}).data)

    @action(detail=False, methods=("GET",))
    def autocomplete(self, request):
        suggestions = []
        try:
            suggestions = AutoCompletion().get_suggestions(
                request.query_params.get("type", "name"), request.query_params.get("q", "")
            )
        except Exception:
            pass

        return Response(status=200, data=suggestions)

    def get_object(self):
        obj = super(PhotoViewSet, self).get_object()

        if (obj.user and obj.user != self.request.user) and obj.visibility != Photo.Visibility.PUBLIC:
            raise PermissionDenied()
        return obj
