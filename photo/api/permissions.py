from rest_framework import permissions
from rest_framework.permissions import SAFE_METHODS


class IsAuthenticatedOrIsReadWrite(permissions.BasePermission):
    def has_permission(self, request, view):
        allowed_methods = ("GET", "POST", "PUT", "PATCH")

        return bool(request.method in allowed_methods or request.user)


class IsOwnerOrIsReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.method in SAFE_METHODS or (request.user and request.user.is_authenticated)

    def has_object_permission(self, request, view, obj):
        return bool(
            request.method in SAFE_METHODS
            or (request.user and request.user.is_authenticated and request.user == obj.user)
        )


class IsOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return request.user == obj.user
