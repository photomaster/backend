from django.db.models import Count, F
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters


class LabelsFilterBackend(DjangoFilterBackend):
    def filter_queryset(self, request, queryset, view):
        if "labels" in request.query_params:
            labels_param = request.query_params["labels"]
            label_titles = labels_param.split(",")
            if len(labels_param.strip()) > 0 and len(label_titles) > 0:
                from django.utils.translation import get_language

                # todo
                # At this point Photo is not registered as a translated model.
                # The Photo queryset is not a MultiLingualQuerySet.
                # Therefore, we have to do the rewrite of the localized field names ourself.
                queryset = queryset.filter(**{f"labels__title_{get_language()}__in": label_titles})
        return queryset


class PhotoFilterBackend(filters.OrderingFilter):
    def filter_queryset(self, request, queryset, view):
        params = request.query_params.get(self.ordering_param)
        if params and params.lstrip("-") == "popularity":
            return queryset.annotate(likes=Count("favoritephoto")).order_by("-likes")

        ordering = self.get_ordering(request, queryset, view)

        if ordering:
            f_ordering = []
            for o in ordering:
                if not o:
                    continue
                # null values should be the last results in either case
                if o[0] == "-":
                    f_ordering.append(F(o[1:]).desc(nulls_last=True))
                else:
                    f_ordering.append(F(o).asc(nulls_last=True))

            queryset = queryset.order_by(*f_ordering)

        return queryset
