from unittest import TestCase
from unittest.mock import patch

from photo.api.serializers import CreatableSlugRelatedField
from photo.locations.models import Label


class CreatableSlugRelatedFieldTest(TestCase):
    def setUp(self):
        self.field = CreatableSlugRelatedField(slug_field="title", queryset=Label.objects.all())
        self.env = patch.dict("os.environ", {"GOOGLE_APPLICATION_CREDENTIALS": ""})

    def test_returns_a_label(self):
        with self.env:
            label = Label.objects.create(title="title_of_label")
            value = self.field.to_internal_value(data=label.title)

            self.assertIsInstance(value, Label)
            self.assertEqual(label.id, value.id, f"should return the label with the title '{label.title}'")

    def test_returns_just_one_result_if_multiple_results_exist(self):
        with self.env:
            title_of_label = "a label"
            Label.objects.bulk_create(
                [Label(title=title_of_label), Label(title=title_of_label), Label(title=title_of_label)]
            )

            value = self.field.to_internal_value(data=title_of_label)

            self.assertIsInstance(value, Label, "should return only one result if multiple objects exist")

    def test_on_access__creates_not_existing_labels(self):
        with self.env:
            title_of_label = "LABEL"

            self.field.to_internal_value(data=title_of_label)

            label = Label.objects.filter(title=title_of_label)
            self.assertIsNotNone(label, "should create a new label if it does not exist")
