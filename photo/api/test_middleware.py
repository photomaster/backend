from unittest import TestCase

from django.test import RequestFactory
from django.utils import translation
from django.utils.translation import get_language

from photo.api.middleware import LocaleMiddleware


class LocaleMiddlewareTestCase(TestCase):
    default_locale = ""

    @classmethod
    def setUpClass(cls) -> None:
        cls.default_locale = get_language()
        cls.request_factory = RequestFactory()

    @classmethod
    def tearDownClass(cls) -> None:
        translation.activate(cls.default_locale)

    def test_process_request__no_query_param(self):
        request = self.request_factory.get("/api/")

        LocaleMiddleware().process_request(request)

        self.assertEqual(get_language(), self.default_locale)

    def test_process_request__locale_query_param(self):
        request = self.request_factory.get("/api/?locale=de")

        LocaleMiddleware().process_request(request)

        self.assertEqual(get_language(), "de")

    def test_process_request__invalid_locale_query_param(self):
        request = self.request_factory.get("/api/?locale=foobar")

        LocaleMiddleware().process_request(request)

        self.assertEqual(get_language(), self.default_locale)
