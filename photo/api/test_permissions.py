from unittest import TestCase

from django.contrib.auth.models import AnonymousUser
from django.test import RequestFactory

from photo.api.permissions import IsOwner
from photo.locations.models import User, Photo


class IsOwnerTest(TestCase):
    def setUp(self):
        self.user = User(username="user")
        self.another_user = User(username="another user")
        self.photo = Photo(user=self.user, name="A photo of the secret plans of the Death Star.")
        self.another_photo = Photo(user=self.another_user, name="A photo of Darth Plagueis")
        self.factory = RequestFactory()

    def testIsOwner_has_permission(self):
        request = self.factory.get("/photos/")
        request.user = self.user

        permission_check = IsOwner()
        self.assertTrue(permission_check.has_permission(request, None))

    def testIsOwner_has_object_permission(self):
        request = self.factory.get("/photos/1")
        request.user = self.user

        permission_check = IsOwner()
        self.assertTrue(permission_check.has_object_permission(request, None, self.photo))

    def testIsOwner_non_owner(self):
        request = self.factory.get("/photos/1")
        request.user = self.user
        permission_check = IsOwner()

        self.assertFalse(permission_check.has_object_permission(request, None, self.another_photo))

    def testIsOwner_not_authenticated(self):
        request = self.factory.get("/photos/1")
        request.user = AnonymousUser()
        permission_check = IsOwner()

        self.assertFalse(permission_check.has_object_permission(request, None, self.photo))
        self.assertFalse(permission_check.has_object_permission(request, None, self.another_photo))
