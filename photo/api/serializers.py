from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned, ValidationError
from django.utils.encoding import smart_text
from rest_framework import serializers
from rest_framework.fields import ImageField

from photo.locations.models import (
    Photo,
    WeatherState,
    Location,
    Label,
    LabelScore,
    Cluster,
    SolarData,
    FavoritePhoto,
    PhotoData,
)


class UserSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.ReadOnlyField()
    password = serializers.CharField(write_only=True)

    def create(self, validated_data):
        user = super().create(validated_data)
        user.set_password(validated_data["password"])
        user.save()

        return user

    def update(self, instance, validated_data):
        user = super().update(instance, validated_data)
        try:
            user.set_password(validated_data["password"])
            user.save()
        except KeyError:
            pass
        return user

    def validate(self, data):
        import django.contrib.auth.password_validation as validators

        if "password" in data:
            password = data.get("password")
            user = get_user_model()(**data)

            try:
                validators.validate_password(password=password, user=user)
            except ValidationError as e:
                raise serializers.ValidationError({"password": list(e.messages)})

        return super(UserSerializer, self).validate(data)

    class Meta:
        model = get_user_model()
        extra_kwargs = {"email": {"write_only": True}}
        fields = [
            "id",
            "url",
            "email",
            "username",
            "password",
            "displayName",
            "description",
            "avatarImage",
            "bannerImage",
        ]


# includes email and groups
class AuthenticatedUserSerializer(UserSerializer):
    groups = serializers.CharField(read_only=True)

    class Meta:
        model = get_user_model()
        fields = UserSerializer.Meta.fields + ["groups"]


class LabelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Label
        fields = ["id", "title"]


class CreatableSlugRelatedField(serializers.SlugRelatedField):
    def to_internal_value(self, data):
        try:
            return self.get_queryset().get_or_create(**{self.slug_field: data})[0]
        except MultipleObjectsReturned:
            return self.get_queryset().filter(**{self.slug_field: data}).first()
        except ObjectDoesNotExist:
            self.fail("does_not_exist", slug_name=self.slug_field, value=smart_text(data))
        except (TypeError, ValueError):
            self.fail("invalid")


class LabelScoreSerializer(serializers.HyperlinkedModelSerializer):
    title = CreatableSlugRelatedField(queryset=Label.objects.all(), slug_field="title", source="label")

    class Meta:
        model = LabelScore
        fields = ["title", "score"]


class PhotoSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.ReadOnlyField()
    externalUsername = serializers.ReadOnlyField()
    externalUrl = serializers.ReadOnlyField()
    licenseUrl = serializers.ReadOnlyField()
    licenseName = serializers.ReadOnlyField()
    labels = LabelScoreSerializer(source="label_photos", many=True, required=False)
    imageThumbnailTeaser = ImageField(read_only=True)
    imageThumbnailTeaserSmall = ImageField(read_only=True)
    imageThumbnailPreview = ImageField(read_only=True)
    user = UserSerializer(read_only=True)

    def update(self, instance, validated_data):
        instance.guid = validated_data.get("guid", instance.guid)
        instance.description = validated_data.get("description", instance.description)
        instance.name = validated_data.get("name", instance.name)
        instance.geo = validated_data.get("geo", instance.geo)
        instance.image = validated_data.get("image", instance.image)
        instance.visibility = validated_data.get("visibility", instance.visibility)
        instance.originalCreationDate = validated_data.get("originalCreationDate", instance.originalCreationDate)

        # instance.currentWeather = validated_data.get('currentWeather')
        # instance.cluster = validated_data.get('cluster')

        labels = list()
        if validated_data.get("label_photos"):
            for label_score_item in validated_data.get("label_photos"):
                if label_score_item.get("id", None):
                    labels.append(label_score_item)
                else:
                    label_score, _ = LabelScore.objects.get_or_create(
                        label=label_score_item.get("label"), score=label_score_item.get("score", 1.0), photo=instance
                    )
                    labels.append(label_score)

        instance.label_photos.set(labels)
        instance.save()

        return instance

    class Meta:
        model = Photo
        fields = [
            "id",
            "url",
            "name",
            "description",
            "geo",
            "photodata",
            "currentWeather",
            "weatherAtCreation",
            "image",
            "imageThumbnailTeaser",
            "imageThumbnailTeaserSmall",
            "imageThumbnailPreview",
            "labels",
            "visibility",
            "originalCreationDate",
            "user",
            "externalUsername",
            "externalUrl",
            "licenseUrl",
            "licenseName",
        ]
        extra_kwargs = {"photodata": {"read_only": True}}
        geo_field = "geo"


class PhotoDataSerializer(serializers.HyperlinkedModelSerializer):
    photo = serializers.HyperlinkedRelatedField(view_name="photo-detail", read_only=True)

    class Meta:
        model = PhotoData
        fields = (
            "id",
            "url",
            "photo",
            "make",
            "model",
            "lensMake",
            "lensModel",
            "focalLength",
            "fNumber",
            "whiteBalance",
            "lightSource",
            "exposureTime",
            "exposureProgram",
            "exposureMode",
            "isoSpeed",
            "flash",
            "pixelXDimension",
            "pixelYDimension",
            "orientation",
            "software",
        )


class PhotoMapSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    imageThumbnailTeaser = ImageField(read_only=True)

    class Meta:
        model = Photo
        fields = ("id", "url", "name", "geo", "imageThumbnailTeaser")


class LocationSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.ReadOnlyField()
    photos = PhotoSerializer(source="photo_set", many=True)

    class Meta:
        model = Location
        fields = ["id", "centroid", "photos"]


class ClusterSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    photos = PhotoMapSerializer(many=True, read_only=True)

    class Meta:
        model = Cluster
        fields = ["id", "centroid", "size", "photos"]


class FavoritePhotoSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.ReadOnlyField()
    photo = PhotoSerializer(many=False)

    class Meta:
        model = FavoritePhoto
        fields = ["id", "url", "photo", "dateAdded"]


class SolarDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = SolarData
        fields = (
            "sunrise",
            "sunset",
            "solarNoon",
            "dayLength",
            "civilTwilightBegin",
            "civilTwilightEnd",
            "nauticalTwilightBegin",
            "nauticalTwilightEnd",
            "astronomicalTwilightBegin",
            "astronomicalTwilightEnd",
            "date",
        )


class WeatherStateSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = WeatherState
        fields = ["id", "url", "locationId", "locationName", "description", "icon", "temperature", "sunrise", "sunset"]


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ["url", "name"]
