from django.conf import settings
from django.middleware.locale import LocaleMiddleware as BaseLocaleMiddleware
from django.utils import translation


class LocaleMiddleware(BaseLocaleMiddleware):
    """
    Override Django LocaleMiddleware in order to read preferred locale from path parameter
    """

    def process_request(self, request):
        if request.GET.get("locale") in settings.LOCALES:
            locale = request.GET.get("locale")
            translation.activate(locale)
        else:
            super(LocaleMiddleware, self).process_request(request)
