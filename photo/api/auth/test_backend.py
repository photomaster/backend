from unittest import TestCase

from django.contrib.auth import get_user_model
from django.http import HttpRequest

from photo.api.auth import EmailBackend


class EmailBackendTest(TestCase):
    def setUp(self):
        self.user = get_user_model()(username="foobar1", email="foobar1@test.de")
        self.user.set_password("1234")
        self.user.save()

    def tearDown(self):
        self.user.delete()

    def test_authenticate(self):
        backend = EmailBackend()
        user = backend.authenticate(HttpRequest(), self.user.email, "1234")
        self.assertIsNotNone(user, "should find a user by his email")
        self.assertEqual(user.username, self.user.username, "should find the correct user")
        self.assertEqual(user.email, self.user.email, "should find the correct user")

    def test_authenticate_by_username(self):
        backend = EmailBackend()
        user = backend.authenticate(HttpRequest(), self.user.username, "1234")
        self.assertIsNone(user, "should not do a lookup based on the username.")

    def test_authenticate_missing_username_param(self):
        backend = EmailBackend()
        user = backend.authenticate(HttpRequest(), username=self.user.email)
        self.assertIsNone(user, "should not return a user if the password parameter is missing.")

    def test_authenticate_missing_password_param(self):
        backend = EmailBackend()
        user = backend.authenticate(HttpRequest(), password="1234")
        self.assertIsNone(user, "should not return a user if the username parameter is missing.")

    def test_authenticate_not_found(self):
        backend = EmailBackend()
        user = backend.authenticate(HttpRequest(), "test@test.de", "1234")
        self.assertIsNone(user, "should not return a user if a user does not exist.")
