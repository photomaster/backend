from rest_framework.pagination import LimitOffsetPagination


class PhotoResultSetPagination(LimitOffsetPagination):
    max_limit = 25
