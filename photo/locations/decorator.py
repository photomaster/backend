import os

from django.conf import settings
from django.db import models
from django.dispatch import receiver
from google.cloud import translate_v2 as translate
from modeltranslation.utils import fallbacks
import logging

logger = logging.getLogger(__name__)


def translate_field(object, fieldname):
    if not os.getenv("GOOGLE_APPLICATION_CREDENTIALS"):
        return

    with fallbacks(False):
        source_language = None
        trans_lang_needed = []
        for locale in settings.LOCALES:
            if getattr(object, f"{fieldname}_{locale}"):
                if source_language is None:
                    source_language = locale
            else:
                trans_lang_needed.append(locale)

        if len(trans_lang_needed) > 0:
            translate_client = translate.Client()
            for locale in trans_lang_needed:
                try:
                    result = translate_client.translate(
                        getattr(object, f"{fieldname}_{source_language}"),
                        target_language=locale,
                        source_language=source_language,
                    )
                    setattr(object, f"{fieldname}_{locale}", result["translatedText"])
                except BaseException:
                    import sys

                    e = sys.exc_info()[0]
                    logger.warning(f"could not translate field {fieldname}_{locale} of object {object.id}: ${e}")


def auto_translate(fieldname):
    def decorator(model):
        assert hasattr(model, fieldname), f"Model has no field {fieldname!r}"

        @receiver(models.signals.pre_save, sender=model, weak=False)
        def pre_save(signal, sender, instance, *args, raw=False, **kwargs):
            if not raw:
                translate_field(instance, fieldname)

        return model

    return decorator
