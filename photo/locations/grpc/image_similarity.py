import grpc

from photo.locations.grpc import image_pb2
from photo.locations.grpc.image_pb2_grpc import ImageSimilarityServiceStub
from photo.locations.models import Photo


def notify_creation(photo: Photo):
    channel = grpc.insecure_channel("imgsim:8080")
    stub = ImageSimilarityServiceStub(channel)
    image = image_pb2.Image(guid=photo.pk, path=photo.image.url, name=photo.name)

    stub.AddImage(image)


def get_visually_similar_photos(photo: Photo, limit):
    if limit <= 0:
        return []

    channel = grpc.insecure_channel("imgsim:8080")

    stub = ImageSimilarityServiceStub(channel)
    image = image_pb2.Image(guid=photo.pk, path=photo.image.url, name=photo.name)
    image_request = image_pb2.ImageRequest(image=image, limit=limit)

    image_similarity_response = stub.GetSimilar(image_request)

    ids = [image_sim.image.guid for image_sim in image_similarity_response.similarities]
    if len(ids) == 0:
        return []

    return Photo.objects.filter(pk__in=ids, visibility=Photo.Visibility.PUBLIC).exclude(id=photo.pk)
