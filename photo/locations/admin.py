from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django.contrib.gis.admin import OSMGeoAdmin

from .models import Photo, WeatherState, Label, LabelScore, SolarData, PhotoData, FavoritePhoto


class LabelScoreInline(admin.TabularInline):
    model = LabelScore
    extra = 2


class LabelAdmin(admin.ModelAdmin):
    inlines = (LabelScoreInline,)
    list_display = ("title",)


class LocationAdmin(OSMGeoAdmin):
    inlines = (LabelScoreInline,)
    list_display = ("guid", "name", "geo", "image", "currentWeather")


# Register your models here.
admin.site.register(Photo, LocationAdmin)
admin.site.register(get_user_model(), UserAdmin)
admin.site.register(Label, LabelAdmin)
admin.site.register(WeatherState)
admin.site.register(SolarData)
admin.site.register(PhotoData)
admin.site.register(FavoritePhoto)
