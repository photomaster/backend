# Generated by Django 3.0.3 on 2020-11-21 15:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0020_auto_20201121_1527'),
    ]

    operations = [
        migrations.AddField(
            model_name='photodata',
            name='lightSource',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
