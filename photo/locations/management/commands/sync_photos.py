import grpc
from django.core.management.base import BaseCommand

from photo.locations.grpc import image_pb2
from photo.locations.grpc.image_pb2_grpc import ImageSimilarityServiceStub
from photo.locations.models import Photo


class Command(BaseCommand):
    help = "Synchronize photo database with imsim grpc service"

    def handle(self, *args, **options):
        channel = grpc.insecure_channel("imgsim:8080")
        stub = ImageSimilarityServiceStub(channel)

        for photo in Photo.objects.filter(visibility=Photo.Visibility.PUBLIC):
            if not photo.image:
                print(f"Skipping {photo.pk} because not image is associated with it")
                continue

            image = image_pb2.Image(guid=photo.pk, path=photo.image.url, name=photo.name)
            try:
                print(f"syncing image {photo.pk}")
                stub.AddImage(image)
            except grpc.RpcError as e:
                print(e)
            except Exception as e:
                print(e)
                return
