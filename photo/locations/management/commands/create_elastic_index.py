from django.conf import settings
from django.core.management import call_command
from django.core.management.base import BaseCommand
from elasticsearch import NotFoundError

from photo.locations.elastic.documents import PhotoDocument


def _rebuild_index():
    call_command("search_index", "--rebuild", "-f")


class Command(BaseCommand):
    help = "Creates elasticsearch photo index if it does not exist."

    def add_arguments(self, parser):
        parser.add_argument("--force", type=bool, default=False)

    def handle(self, *args, **options):
        if "django_elasticsearch_dsl" not in settings.INSTALLED_APPS:
            return

        if "force" in options and options["force"] is True:
            _rebuild_index()
        else:
            try:
                PhotoDocument().search().count()
            except NotFoundError:
                _rebuild_index()
