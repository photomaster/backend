from django.core.management import call_command
from django.core.management.base import BaseCommand

from photo.locations.models import Photo
from photo.locations.tasks import _extract_exif_data, _rotate_image


class Command(BaseCommand):
    help = "Extracts exif data from imported photos and updates elastic index."

    def handle(self, *args, **options):
        queryset = Photo.objects.filter(externalUrl__isnull=False, photodata__isnull=True)

        for photo in queryset:
            _extract_exif_data(photo)
            _rotate_image(photo)

        call_command("search_index", "--rebuild", "-f")
