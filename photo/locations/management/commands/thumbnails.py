from os import path

from django.core.management.base import BaseCommand
from imagekit.cachefiles import ImageCacheFile

from photo.locations.models import Photo


def _cached_generate(image: ImageCacheFile):
    if path.exists(image.file.name):
        return
    image.generate()


class Command(BaseCommand):
    help = "Generates all photo thumbnails"

    def handle(self, *args, **options):
        total = Photo.objects.count()
        batch_size = 25
        offset = 0

        while offset < total:
            print(f"Processing {offset}/{total}")
            for photo in Photo.objects.all()[offset : offset + batch_size]:
                _cached_generate(photo.imageThumbnailPreview)
                _cached_generate(photo.imageThumbnailTeaser)
                _cached_generate(photo.imageThumbnailTeaserSmall)

            offset += batch_size
