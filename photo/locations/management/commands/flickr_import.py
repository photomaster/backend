from tempfile import NamedTemporaryFile
from defusedxml import ElementTree
import requests
from django.conf import settings
from django.contrib.gis.geos import Point
from django.core.files import File
from django.core.management.base import BaseCommand, CommandError

from photo.locations.models import Photo

flickr_licences = {
    "All Rights Reserved": 0,
    "Attribution-NonCommercial-ShareAlike License": 1,
    "Attribution-NonCommercial License": 2,
    "Attribution-NonCommercial-NoDerivs License": 3,
    "Attribution License": 4,
    "Attribution-ShareAlike License": 5,
    "Attribution-NoDerivs License": 6,
    "No known copyright restrictions": 7,
    "United States Government Work": 8,
    "Public Domain Dedication (CC0)": 9,
    "Public Domain Mark": 10,
}

licences_to_use = (
    "Public Domain Mark",
    "Public Domain Dedication (CC0)",
    "Attribution License",
    "Attribution-NonCommercial License",
)


class Command(BaseCommand):
    help = "import images from flickr related to a specific topic"

    def add_arguments(self, parser):
        parser.add_argument("search", type=str)
        parser.add_argument("-p", "--page", default=1, type=int)

    def handle(self, *args, **options):
        licence_ids = ",".join(str(flickr_licences[key]) for key in licences_to_use)

        url = (
            "https://api.flickr.com/services/rest?"
            f"method=flickr.photos.search&api_key={settings.FLICKR_API_KEY}"
            f"&content_type=1&has_geo=1&license={licence_ids}&safe_search=1&geo_context=2&media=photos&"
            f"text={options['search']}&page={options['page']}"
            "&extras=original_format,geo,tags,date_taken,owner_name,description"
            "&sort=relevance"
        )
        response = requests.get(url)

        if response.status_code != 200:
            raise CommandError("could not read from flickr api")

        tree = ElementTree.fromstring(response.content)
        for photo in tree[0]:
            attr = photo.attrib
            if "originalsecret" not in attr:
                print("skipping non creative common photo " + attr["id"])
                continue

            flickr_post_url = "https://flickr.com/photos/{}/{}".format(attr["owner"], attr["id"])

            photo = Photo(
                name=attr["title"],
                guid=attr["owner"] + "/" + attr["id"],
                geo=Point(float(attr["longitude"]), float(attr["latitude"])),
                externalUrl=flickr_post_url,
                externalUsername=attr["ownername"],
                originalCreationDate=attr["datetaken"],
                visibility=Photo.Visibility.PUBLIC,
            )
            try:
                photo.save()

                image_url = "https://live.staticflickr.com/{}/{}_{}_o_d.{}".format(
                    attr["server"], attr["id"], attr["originalsecret"], attr["originalformat"]
                )
                r = requests.get(image_url)

                img_temp = NamedTemporaryFile(delete=True)
                img_temp.write(r.content)
                img_temp.flush()
                filename = "flickr-" + attr["owner"] + "-" + attr["id"] + "." + attr["originalformat"]
                photo.image.save(filename, File(img_temp))
            except Exception as e:
                print(f"error importing file {attr['id']}: ", e)
