import json

import requests
from django.conf import settings
from django.core.management.base import BaseCommand

from photo.locations.models import Photo

license_infos = {
    "2": ("Attribution-NonCommercial License", "https://creativecommons.org/licenses/by-nc/2.0/"),
    "4": ("Attribution License", "https://creativecommons.org/licenses/by/2.0/"),
    "9": ("Public Domain Dedication (CC0)", "https://creativecommons.org/publicdomain/zero/1.0/"),
    "10": ("Public Domain Mark", "https://creativecommons.org/publicdomain/mark/1.0/"),
}


class Command(BaseCommand):
    help = "Import licenses of photos from flickr."

    def handle(self, *args, **options):
        photos = Photo.objects.filter(externalUrl__isnull=False, licenseName__isnull=True)
        print(f"Found {photos.count()} photos without license data.")

        for i, photo in enumerate(photos):
            if i % 100 == 0:
                print(f"{i}/{photos.count()}")
            self.import_license_data(photo)

    def import_license_data(self, photo: Photo):
        url = self.get_flickr_api_url(photo)

        response = requests.get(url)
        if not response.ok:
            print(f"Could not fetch license data of photo {photo.id}!")
            return

        data = json.loads(response.content)
        if "photo" not in data or "license" not in data["photo"]:
            print(f"Could not extract license of photo {photo.id} from response!")
            return

        license = data["photo"]["license"]
        if license not in license_infos:
            print(f"Photo {photo.id} is not distributed under a valid license '{license}'!")
            return

        photo.licenseName, photo.licenseUrl = license_infos[license]
        photo.save()

    @staticmethod
    def get_flickr_api_url(photo: Photo):
        photo_flickr_id = photo.externalUrl.split("/")[-1]
        url = (
            "https://www.flickr.com/services/rest/?method=flickr.photos.getInfo"
            "&api_key={}"
            "&photo_id={}"
            "&format=json"
            "&nojsoncallback=1"
        )
        return url.format(settings.FLICKR_API_KEY, photo_flickr_id)
