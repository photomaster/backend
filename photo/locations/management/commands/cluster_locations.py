from django.core.management.base import BaseCommand

from photo.locations.service.map import generate_detail_clusters
from photo.locations.tasks import location


class Command(BaseCommand):
    help = "Regenerate all location clusters."

    # def add_arguments(self, parser):
    #   parser.add_argument('poll_ids', nargs='+', type=int)

    def handle(self, *args, **options):
        location.generate_location_clusters()
        generate_detail_clusters()
