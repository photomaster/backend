import time

from django.core.management.base import BaseCommand

from photo.locations.models import Photo
from photo.locations.service.weather import update_current_weather


class Command(BaseCommand):
    help = "Update weather state of all locations."

    def handle(self, *args, **options):
        locations = Photo.objects.all()
        print(f"Updating {len(locations)} locations.")
        for location in locations:
            update_current_weather(location)
            time.sleep(1)
