from django.core.management.base import BaseCommand

from photo.locations.models import Photo
from photo.locations.service import photo


class Command(BaseCommand):
    help = "Classify an image of a photo object using the google vision api."

    def add_arguments(self, parser):
        parser.add_argument("photo_id", type=int)

    def handle(self, *args, **options):
        try:
            photo_object = Photo.objects.get(pk=options["photo_id"])
            photo.classify(photo_object)
        except Exception as e:
            print(e)
