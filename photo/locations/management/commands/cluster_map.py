from django.core.management.base import BaseCommand

from photo.locations.service.map import generate_detail_clusters


class Command(BaseCommand):
    help = "Regenerate all hierarchical map clusters."

    def handle(self, *args, **options):
        generate_detail_clusters()
