from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser
from django.contrib.gis.db import models
from django.utils import timezone
from imagekit.models import ImageSpecField, ProcessedImageField
from imagekit.processors import ResizeToFill, ResizeToFit

from photo.locations.decorator import auto_translate


# this is the auth user class
class User(AbstractUser):
    email = models.EmailField(unique=True)
    displayName = models.CharField(max_length=255, null=True, blank=True)
    avatarImage = ProcessedImageField(
        upload_to="avatars", null=True, blank=True, processors=(ResizeToFit(width=120, height=120),)
    )
    bannerImage = models.ImageField(upload_to="banners", null=True, blank=True)
    description = models.TextField(null=True, blank=True)


class OwnedModel(models.Model):
    class Meta:
        abstract = True

    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, null=True, blank=True)


class Location(models.Model):
    centroid = models.PointField()


class WeatherState(OwnedModel):
    locationId = models.IntegerField(null=True)
    locationName = models.CharField(max_length=255, null=True)
    description = models.CharField(max_length=255, null=True, blank=True)
    icon = models.CharField(max_length=10, null=True, blank=True)
    temperature = models.FloatField(null=True, blank=True)
    sunrise = models.TimeField(null=True, blank=True)
    sunset = models.TimeField(null=True, blank=True)
    updated_at = models.DateTimeField(default=timezone.now, blank=True)


@auto_translate("title")
class Label(models.Model):
    title = models.CharField(max_length=255, null=False, blank=True)
    mid = models.CharField(max_length=100)


class LabelScore(models.Model):
    score = models.FloatField(default=1.0)
    label = models.ForeignKey("Label", related_name="label_photos", on_delete=models.SET_NULL, null=True)
    photo = models.ForeignKey("Photo", related_name="label_photos", on_delete=models.SET_NULL, null=True, blank=True)


class Photo(OwnedModel):
    class Visibility(models.IntegerChoices):
        DRAFT = 1
        PRIVATE = 2
        PUBLIC = 3

    def save(self, *args, **kwargs):
        if not self.originalCreationDate:
            self.originalCreationDate = timezone.now()
        return super().save(*args, **kwargs)

    guid = models.CharField(max_length=255, null=True, unique=True)
    name = models.CharField(max_length=200, blank=True)
    description = models.TextField(blank=True)
    originalCreationDate = models.DateTimeField(null=True)
    geo = models.PointField(null=True, blank=True)
    labels = models.ManyToManyField(Label, through=LabelScore, blank=True)

    image = models.ImageField(upload_to="locations", blank=True, null=True)
    imageThumbnailTeaser = ImageSpecField(
        source="image", processors=[ResizeToFill(345, 200)], format="JPEG", options={"quality": 80}
    )

    imageThumbnailTeaserSmall = ImageSpecField(
        source="image", processors=[ResizeToFill(120, 120)], format="JPEG", options={"quality": 60}
    )

    imageThumbnailPreview = ImageSpecField(
        source="image", processors=[ResizeToFit(1000, 1000)], format="JPEG", options={"quality": 85}
    )

    visibility = models.IntegerField(choices=Visibility.choices, default=Visibility.DRAFT)
    currentWeather = models.ForeignKey(WeatherState, on_delete=models.SET_NULL, null=True, blank=True)
    weatherAtCreation = models.OneToOneField(
        WeatherState, on_delete=models.CASCADE, null=True, blank=True, related_name="photo_creation"
    )
    cluster = models.ForeignKey(Location, on_delete=models.SET_NULL, null=True, blank=True)

    externalUsername = models.CharField(max_length=255, blank=True)
    externalUrl = models.URLField(blank=True, null=True)
    licenseUrl = models.URLField(blank=True, null=True)
    licenseName = models.CharField(max_length=50, blank=True, null=True)


class PhotoData(OwnedModel):
    photo = models.OneToOneField(Photo, on_delete=models.CASCADE, null=True, blank=True)
    make = models.CharField(max_length=256, null=True, blank=True)
    model = models.CharField(max_length=256, null=True, blank=True)
    lensMake = models.CharField(max_length=256, null=True, blank=True)
    lensModel = models.CharField(max_length=256, null=True, blank=True)
    focalLength = models.CharField(max_length=25, null=True, blank=True)  # Brennweite
    fNumber = models.CharField(max_length=25, null=True, blank=True)  # Blendenzahl
    whiteBalance = models.CharField(max_length=100, null=True, blank=True)  # Weißabgleich
    lightSource = models.CharField(max_length=100, null=True, blank=True)
    exposureTime = models.CharField(max_length=25, null=True, blank=True)  # Verschlusszeit
    exposureProgram = models.CharField(max_length=25, null=True, blank=True)
    exposureMode = models.CharField(max_length=25, null=True, blank=True)
    isoSpeed = models.CharField(max_length=25, null=True, blank=True)
    flash = models.CharField(max_length=25, null=True, blank=True)
    pixelXDimension = models.FloatField(null=True, blank=True)
    pixelYDimension = models.FloatField(null=True, blank=True)
    orientation = models.CharField(max_length=100, null=True, blank=True)
    software = models.CharField(max_length=256, null=True, blank=True)


class SolarData(models.Model):
    sunrise = models.DateTimeField()
    sunset = models.DateTimeField()
    solarNoon = models.DateTimeField()
    dayLength = models.IntegerField()
    civilTwilightBegin = models.DateTimeField()
    civilTwilightEnd = models.DateTimeField()
    nauticalTwilightBegin = models.DateTimeField()
    nauticalTwilightEnd = models.DateTimeField()
    astronomicalTwilightBegin = models.DateTimeField()
    astronomicalTwilightEnd = models.DateTimeField()
    photo = models.ForeignKey(Photo, on_delete=models.CASCADE)
    date = models.DateField()

    class Meta:
        unique_together = (
            (
                "photo",
                "date",
            ),
        )


class FavoritePhoto(OwnedModel):
    photo = models.ForeignKey(Photo, on_delete=models.CASCADE)
    dateAdded = models.DateTimeField(auto_now_add=True, blank=True)


class Cluster(models.Model):
    detailLevel = models.IntegerField(db_index=True)
    centroid = models.PointField()
    size = models.IntegerField(default=0)
    photos = models.ManyToManyField(Photo, blank=True, related_name="detail_cluster")
