import json
import os

import requests
from django.contrib.gis.geos import Point
from django.utils import timezone
from django.utils.translation import gettext as _
from google.cloud import vision, vision_v1p4beta1
from google.cloud.vision_v1.proto import image_annotator_pb2
from rest_framework.exceptions import APIException

from photo.locations.grpc.image_similarity import get_visually_similar_photos
from photo.locations.models import Photo, LabelScore, Label, SolarData
import logging

logger = logging.getLogger(__name__)


class SaveSearchException(APIException):
    status_code = 400

    def __init__(self, message):
        self.detail = message


def classify(photo: Photo):
    if not photo.image:
        return

    response = _get_vision_api_response(photo.image.path)

    if response.safe_search_annotation.adult >= 3:
        raise SaveSearchException(_("image.upload.adult_content"))
    if response.safe_search_annotation.violence >= 3:
        raise SaveSearchException(_("image.upload.violent_content"))

    if not photo.geo and len(response.landmark_annotations) > 0:
        landmark = response.landmark_annotations[0]
        photo.geo = Point(landmark.locations[0].lat_lng.longitude, landmark.locations[0].lat_lng.latitude)

    if len(response.label_annotations) > 0:
        photo.label_photos.clear()
        for label_annotation in response.label_annotations:
            label, created = Label.objects.get_or_create(
                title_en=label_annotation.description, mid=label_annotation.mid
            )
            label_score = LabelScore.objects.create(photo=photo, label=label, score=label_annotation.score)
            photo.label_photos.add(label_score)

    photo.save(update_fields=("label_photos", "geo"))
    return photo


class MultiCallableStub(object):
    """Stub for the grpc.UnaryUnaryMultiCallable interface."""

    def __init__(self, method, channel_stub):
        self.method = method
        self.channel_stub = channel_stub

    def __call__(self, request, timeout=None, metadata=None, credentials=None):
        self.channel_stub.requests.append((self.method, request))

        response = None
        if self.channel_stub.responses:
            response = self.channel_stub.responses.pop()

        if isinstance(response, Exception):
            raise response

        if response:
            return response


class ChannelStub(object):
    """Stub for the grpc.Channel interface."""

    def __init__(self, responses=[]):
        self.responses = responses
        self.requests = []

    def unary_unary(self, method, request_serializer=None, response_deserializer=None):
        return MultiCallableStub(method, self)


# todo: replace with real api call
def _get_vision_api_response(image_full_path):
    if os.getenv("GOOGLE_APPLICATION_CREDENTIALS"):
        with open(image_full_path, "rb") as image_file:
            client = vision.ImageAnnotatorClient()
            response = client.annotate_image(
                {
                    "image": image_file,
                    "features": [
                        {"type": vision.enums.Feature.Type.LANDMARK_DETECTION},
                        {"type": vision.enums.Feature.Type.IMAGE_PROPERTIES},
                        {"type": vision.enums.Feature.Type.LABEL_DETECTION},
                        {"type": vision.enums.Feature.Type.SAFE_SEARCH_DETECTION},
                    ],
                }
            )
            return response
    else:
        from unittest import mock

        with open(".test/google_vision_response.json") as mock_response:
            expected_response = image_annotator_pb2.BatchAnnotateImagesResponse(
                **{"responses": [json.loads(mock_response.read())]}
            )
            # Mock the API response
            channel = ChannelStub(responses=[expected_response])
            patch = mock.patch("google.api_core.grpc_helpers.create_channel")
            with patch as create_channel:
                create_channel.return_value = channel
                client = vision_v1p4beta1.ImageAnnotatorClient()

            return client.batch_annotate_images([]).responses[0]


def get_similiar_photos(photo: Photo, limit=15):
    return get_visually_similar_photos(photo, limit)


def get_solar_data(photo_id, date=timezone.now().strftime("%Y-%m-%d")):
    photo = Photo.objects.get(pk=photo_id)
    if not photo:
        return

    solar_data = SolarData.objects.filter(photo=photo, date=date).first()
    if not solar_data:
        r = requests.get(
            f"https://api.sunrise-sunset.org/json?lat={photo.geo.y}&lng={photo.geo.x}%date={date}&formatted=0"
        )
        if not r.ok:
            logger.warning(f"could not load sunrise data from api.sunrise-sunset.org: ({r.status_code}) {r.text}")
            return None
        data = r.json()
        solar_data = SolarData(
            sunrise=data["results"]["sunrise"],
            sunset=data["results"]["sunset"],
            solarNoon=data["results"]["solar_noon"],
            dayLength=data["results"]["day_length"],
            civilTwilightBegin=data["results"]["civil_twilight_begin"],
            civilTwilightEnd=data["results"]["civil_twilight_end"],
            nauticalTwilightBegin=data["results"]["nautical_twilight_begin"],
            nauticalTwilightEnd=data["results"]["nautical_twilight_end"],
            astronomicalTwilightBegin=data["results"]["nautical_twilight_begin"],
            astronomicalTwilightEnd=data["results"]["nautical_twilight_end"],
            photo=photo,
            date=date,
        )
        solar_data.save()
    return solar_data
