from datetime import timedelta, datetime

import requests
from django.utils import timezone

from photo.config.settings.base import OPEN_WEATHERMAP_API_KEY
from photo.locations.models import Photo, WeatherState


def _fetch_api_data(lon, lat):
    url = (
        "http://api.openweathermap.org/data/2.5/weather?"
        + f"lat={lat}&lon={lon}"
        + f"&APPID={OPEN_WEATHERMAP_API_KEY}&units=metric"
    )

    r = requests.get(url)
    if r.status_code >= 300 or r.status_code < 200:
        raise Exception(f"Failed to fetch weather state. Http status ${r.status_code}", lat, lon)

    return r.json()


def _update_weather_state(json: dict, weather_state=None):
    if not weather_state:
        weather_state = WeatherState()

    weather_state.locationId = json["id"]
    weather_state.locationName = json["name"]
    weather_state.description = json["weather"][0]["description"]
    weather_state.icon = json["weather"][0]["icon"]
    weather_state.temperature = json["main"]["temp"]
    weather_state.sunrise = datetime.utcfromtimestamp(json["sys"]["sunrise"]).time()
    weather_state.sunset = datetime.utcfromtimestamp(json["sys"]["sunset"]).time()

    return weather_state


def add_weather_at_creation(photo: Photo):
    if photo.weatherAtCreation and photo.weatherAtCreation.locationId:
        return photo.weatherAtCreation

    if (
        not photo.originalCreationDate
        or not photo.geo
        or not photo.geo.x
        or not photo.geo.y
        or photo.originalCreationDate.date() != datetime.today().date()
    ):
        if not photo.weatherAtCreation:
            photo.weatherAtCreation = WeatherState(user=photo.user)
            photo.weatherAtCreation.save()
            photo.save(update_fields=("weatherAtCreation",))
        return photo.weatherAtCreation

    data = _fetch_api_data(photo.geo.x, photo.geo.y)
    photo.weatherAtCreation = _update_weather_state(data)
    photo.weatherAtCreation.user = photo.user
    photo.weatherAtCreation.save()
    photo.save(update_fields=("weatherAtCreation",))
    return photo.weatherAtCreation


def update_current_weather(photo: Photo, max_age_hours=12):
    if not photo.geo or not photo.geo.x or not photo.geo.y:
        return

    if photo.currentWeather and (timezone.now() - photo.currentWeather.updated_at) < timedelta(hours=max_age_hours):
        return photo.currentWeather

    json = _fetch_api_data(photo.geo.x, photo.geo.y)
    weather_state = _update_weather_state(json, photo.currentWeather)
    weather_state.updated_at = timezone.now()
    weather_state.save()
    if photo.currentWeather != weather_state:
        photo.currentWeather = weather_state
        photo.save(update_fields=("currentWeather",))

    return weather_state
