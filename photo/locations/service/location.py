import urllib

import requests
from django.contrib.gis.geos import fromstr
from django.db import connection

from photo.locations.models import Photo, Location


def generate_location_clusters():
    with connection.cursor() as cursor:
        query = "SELECT\
                    array_agg(id order by id asc) as photo_ids,\
                    ST_asText(st_centroid(st_union(geo))) as centroid\
                FROM locations_photo, (\
                    SELECT unnest(ST_ClusterWithin(geo, 0.008)) AS cluster FROM locations_photo\
                ) as location_clustered\
                 WHERE ST_Contains(ST_CollectionExtract(cluster, 1), geo)\
                GROUP BY cluster"

        cursor.execute(query)

        Location.objects.all().delete()

        for row in cursor.fetchall():
            # print(f"Clustering {len(row[0])} locations with centroid {row[1]}.")

            centroid = fromstr(row[1])
            locations = Photo.objects.filter(pk__in=row[0])

            location = Location.objects.filter(centroid=centroid).first()
            if not location:
                location = Location(centroid=centroid)
                location.save()

            location.photo_set.set(locations)
            location.save()

        cursor.close()

        return Location.objects.all()


def location_name_to_gps(location_name) -> tuple:
    from django.core.cache import cache

    redis_key = "location_nomination_" + location_name
    lat_long = cache.get(redis_key)

    if lat_long is None:
        url = "https://nominatim.openstreetmap.org/search/?q={}&format=json".format(urllib.parse.quote(location_name))
        response = requests.get(url, headers={"User-Agent": "photomaster.rocks"})
        if response.status_code != 200:
            raise RuntimeError("could not read from openstreetmap.org api!")

        json = response.json()
        if not json or len(json) == 0:
            cache.set(redis_key, False, timeout=60 * 60)
            raise RuntimeError("location not found")

        # first item should have the highest importance
        lat, long = json[0]["lat"], json[0]["lon"]
        lat_long = "{}:{}".format(lat, long)

        # cache value for one hour
        cache.set(redis_key, lat_long, timeout=60 * 60)
    else:
        if lat_long is False:
            raise RuntimeError("location not found")

        p = lat_long.split(":")
        if len(p) != 2:
            raise RuntimeError("location not found")

        lat, long = (p[0]), p[1]

    return float(lat), float(long)
