from django.contrib.gis.geos import Point, GeometryCollection, fromstr
from django.db import connection, transaction
from django.db.models import Max

from photo.locations.models import Photo, Cluster


def naive_km_to_degrees(km):
    return km / 40075 * 360


def get_entries(lat1, long1, lat2, long2, zoom=9):
    # select the biggest zoom level the cluster table offers
    r = Cluster.objects.filter(detailLevel__lte=zoom).aggregate(zoom=Max("detailLevel"))
    zoom = r["zoom"]

    lat1, long1, lat2, long2 = float(lat1), float(long1), float(lat2), float(long2)
    top_left = Point(
        long1,
        lat1,
    )
    bottom_left = Point(long2, lat1)
    top_right = Point(long1, lat2)
    bottom_right = Point(long2, lat2)

    # rectangular viewport shape
    poly = GeometryCollection(top_left, bottom_left, top_right, bottom_right)
    return Cluster.objects.filter(centroid__contained=poly, detailLevel=zoom).prefetch_related("photos")


def generate_detail_clusters():
    min_zoom = 3
    max_zoom = 17
    max_cluster_size_to_save_photos = 5

    with connection.cursor() as cursor:
        for zoom in range(min_zoom, max_zoom + 1):
            if zoom >= 9:
                earth_circumference = 40075
                visible_width = earth_circumference / (2 ** (zoom - min_zoom))
                cluster_width = naive_km_to_degrees(visible_width / 100)

                query = """SELECT
                            count(id) as photo_count,
                            case when count(id) <= %s then array_agg(id)
                            else null end as photo_ids,
                            ST_asText(st_centroid(st_union(geo))) as centroid
                        FROM locations_photo, (
                            SELECT unnest(ST_ClusterWithin(geo, %s)) AS cluster
                            FROM locations_photo
                            WHERE geo IS NOT NULL AND visibility = 3
                        ) as location_clustered
                        WHERE ST_Contains(ST_CollectionExtract(cluster, 1), geo)
                        GROUP BY cluster"""
            else:
                if zoom == 3:
                    cluster_width = 1.25
                if zoom == 4:
                    cluster_width = 1
                if zoom == 5:
                    cluster_width = 0.50
                if zoom == 6:
                    cluster_width = 0.35
                if zoom == 7:
                    cluster_width = 0.2
                if zoom == 8:
                    cluster_width = 0.1
                if zoom == 9:
                    cluster_width = 0.05

                query = """select
                            count(c.id) as photo_count,
                            case when count(id) <= %s then array_agg(id)
                            else null end as photo_ids,
                            ST_asText(ST_Centroid(ST_Collect(c.geo))) as centroid
                        from (
                            SELECT
                                id, geo, ST_ClusterDBSCAN(geo, eps := %s, minpoints := 2) over () AS cid
                            FROM locations_photo WHERE geo IS NOT NULL AND visibility = 3
                        )c
                        group by c.cid"""

            cursor.execute(query, [max_cluster_size_to_save_photos, cluster_width])

            with transaction.atomic():
                Cluster.objects.filter(detailLevel=zoom).delete()
                for row in cursor.fetchall():
                    centroid = fromstr(row[2])
                    cluster = Cluster(detailLevel=zoom, centroid=centroid, size=row[0])
                    cluster.save()
                    if row[1]:
                        cluster.photos.set(Photo.objects.filter(pk__in=row[1]))
