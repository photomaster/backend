import json
from unittest import TestCase

from elasticsearch_dsl import Search

from photo.locations.elastic.search import photo_search


class PhotoSearchTestCase(TestCase):
    def test_photo_search__return_value(self):
        search_document = photo_search()
        self.assertIsInstance(search_document, Search)

    def test_photo_search__season_filter(self):
        data = json.dumps(photo_search(season="winter").to_dict())

        self.assertIn('{"month":', data)
        self.assertIn("range", data)

    def test_photo_search__weather_filter(self):
        data = json.dumps(photo_search(weather="rain").to_dict())

        self.assertIn('"weatherAtCreation.description": "rain"', data)

    def test_photo_search__temperature_filter(self):
        data = json.dumps(photo_search(temperatureMin=0, temperatureMax=30).to_dict())

        self.assertIn('"weatherAtCreation.temperature"', data)
        self.assertIn('"gte": 0', data)
        self.assertIn('"lt": 30', data)

    def test_photo_search__photodata_fields_filter(self):
        data = json.dumps(photo_search(model="iPhone").to_dict())

        self.assertIn('"photodata.model"', data)
        self.assertIn('"iPhone"', data)

    def test_photo_search__query(self):
        data = json.dumps(photo_search(q="foobar").to_dict())

        self.assertIn('"name.keyword"', data)
        self.assertIn('"foobar"', data)
