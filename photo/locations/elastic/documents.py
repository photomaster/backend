from django_elasticsearch_dsl import Document
from django_elasticsearch_dsl import fields
from django_elasticsearch_dsl.registries import registry
from elasticsearch_dsl import analyzer
from elasticsearch_dsl.analysis import tokenizer, token_filter

from photo.locations.models import Photo

edge_ngram_tokenizer = tokenizer("ngram", type="edge_ngram", min_gram=3, max_gram=20, token_chars=("letter", "digit"))

edge_ngram_analyzer = analyzer("edge_ngram_analyzer", tokenizer=edge_ngram_tokenizer, filter=["lowercase"])

edge = token_filter("edge", type="edge_ngram", min_gram=3, max_gram=20, token_chars=("letter", "digit"))

reverse_edge_analyzer = analyzer(
    "reverse_analyzer", tokenizer="standard", filter=["lowercase", "reverse", edge, "reverse"]
)

lower_keyword_analyzer = analyzer("lower_keyword_analyzer", tokenizer="keyword", filter=["lowercase"])

word_delimiter_analyzer = analyzer(
    "word_delimiter_analyzer", tokenizer="standard", filter=["lowercase", "word_delimiter"]
)

snowball_english_filter = token_filter("english", type="snowball", language="English")
snowball_german_filter = token_filter("german", type="snowball", language="German")
snowball_german_analyzer = analyzer(
    "snowball_german_analyzer", tokenizer="standard", filter=["lowercase", snowball_german_filter]
)
snowball_english_analyzer = analyzer(
    "snowball_english_analyzer", tokenizer="standard", filter=["lowercase", snowball_english_filter]
)


@registry.register_document
class PhotoDocument(Document):
    labels_en = fields.TextField(
        fields={
            "stemmed": fields.TextField(analyzer=snowball_english_analyzer),
            "suggest": fields.CompletionField(analyzer=lower_keyword_analyzer),
        }
    )
    labels_de = fields.TextField(
        fields={
            "stemmed": fields.TextField(analyzer=snowball_german_analyzer),
            "suggest": fields.CompletionField(analyzer=lower_keyword_analyzer),
        }
    )
    geo = fields.GeoPoint()
    month = fields.IntegerField()
    photodata = fields.NestedField(
        properties={
            "model": fields.TextField(fields={"suggest": fields.CompletionField(analyzer=lower_keyword_analyzer)}),
            "make": fields.TextField(
                fields={
                    "suggest": fields.CompletionField(analyzer=lower_keyword_analyzer),
                }
            ),
            "software": fields.TextField(),
        }
    )
    weatherAtCreation = fields.NestedField(
        properties={"temperature": fields.FloatField(), "description": fields.TextField()}
    )

    name = fields.TextField(
        fields={
            "keyword": fields.TextField(analyzer=word_delimiter_analyzer),
            "ngram": fields.TextField(analyzer=edge_ngram_analyzer),
            "ngram_reverse": fields.TextField(analyzer=reverse_edge_analyzer),
            "suggest": fields.CompletionField(analyzer=lower_keyword_analyzer),
        }
    )
    originalCreationDate = fields.DateField(fields={"formatted": fields.TextField(analyzer="whitespace")})

    def prepare_labels_en(self, instance):
        return [label.title_en for label in instance.labels.all()]

    def prepare_labels_de(self, instance):
        return [label.title_de for label in instance.labels.all()]

    def prepare_month(self, instance):
        return int(instance.originalCreationDate.strftime("%m")) if instance.originalCreationDate else -1

    def get_queryset(self):
        return (
            super(PhotoDocument, self)
            .get_queryset()
            .filter(visibility=Photo.Visibility.PUBLIC)
            .select_related("photodata")
            .prefetch_related("label_photos__label")
        )

    class Index:
        name = "photos"
        settings = {"number_of_shards": 1, "number_of_replicas": 0}

    class Django:
        model = Photo

        description = fields.TextField()

        fields = ["description"]

        # Ignore auto updating of Elasticsearch when a model is saved
        # or deleted:
        ignore_signals = True

        # Don't perform an index refresh after every update (overrides global setting):
        # auto_refresh = False

        # Paginate the django queryset used to populate the index with the specified size
        # (by default it uses the database driver's default setting)
        # queryset_pagination = 5000
