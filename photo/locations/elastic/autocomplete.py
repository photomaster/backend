from django.utils.translation import get_language
from django.core.cache import cache

from photo.locations.elastic.documents import PhotoDocument


class AutoCompletion:
    VALID_FIELDS = ("name", "photodata.model", "photodata.make", "labels")
    CACHE_TTL = 60 * 2

    def get_suggestions(self, field_name, query):
        if field_name not in self.VALID_FIELDS:
            field_name = "name"

        if field_name == "labels":
            field_name += "_" + get_language()

        cache_key = self._get_cache_key(field_name, query)
        suggestions = cache.get(cache_key)
        if suggestions is None:
            suggestions = self._fetch_suggestions(field_name, query)
            cache.set(cache_key, suggestions, timeout=self.CACHE_TTL)

        return suggestions

    @staticmethod
    def _fetch_suggestions(field_name, query):
        search_document = (
            PhotoDocument.search()
            .suggest("name", text=query, completion={"field": field_name + ".suggest", "skip_duplicates": True})
            .source(["name"])
        )

        return [suggestion.text for suggestion in search_document.execute().suggest.name[0].options]

    @staticmethod
    def _get_cache_key(field_name, value):
        return "autocomplete_" + field_name + "_" + value
