from elasticsearch_dsl import Search
from elasticsearch_dsl.query import Bool, Match, MultiMatch, MatchPhrase, Wildcard, Nested, Range, Q, MatchAll

from photo.locations.elastic.documents import PhotoDocument


def photo_search(**kwargs) -> Search:
    filters = []

    search_document = PhotoDocument.search().filter(MatchAll())
    if "q" in kwargs and kwargs["q"] and len(kwargs["q"]) > 0:
        search_document = _get_fulltext_search_document(kwargs["q"])

    if "weather" in kwargs and kwargs["weather"]:
        filters.append(_get_filter_weather(kwargs))

    if "season" in kwargs and kwargs["season"] in (
        "winter",
        "spring",
        "summer",
        "fall",
    ):
        filters.append(_get_filter_season(kwargs))

    if "temperatureMin" in kwargs and kwargs["temperatureMax"]:

        def parse_int(v, fallback):
            try:
                return int(v)
            except ValueError:
                return fallback

        min_temperature = parse_int(kwargs["temperatureMin"], 0)
        max_temperature = parse_int(kwargs["temperatureMax"], 50)

        filters.append(_get_filter_range("weatherAtCreation.temperature", min_temperature, max_temperature))

    other_filters = {
        k: v
        for k, v in kwargs.items()
        if v and k not in ("q", "weather", "season", "locale", "offset", "temperatureMin", "temperatureMax")
    }
    if len(other_filters) > 0:
        filters.append(_get_filter_photodata_fields(other_filters))

    if len(filters) > 0:
        search_document = search_document.filter(Bool(must=filters))

    return search_document


def _season_to_range(season):
    if season == "winter":
        return [{"gte": 11, "lte": 12}, {"gte": 1, "lte": 2}]
    if season == "spring":
        return [{"gte": 3, "lte": 5}]
    if season == "summer":
        return [{"gte": 5, "lte": 9}]
    return [{"gte": 9, "lte": 11}]


def _get_filter_photodata_fields(other_filters: dict):
    return Bool(
        must=[Nested(path="photodata", query=MatchPhrase(**{"photodata." + k: v})) for k, v in other_filters.items()]
    )


def _get_filter_range(field, min, max):
    return Nested(path="weatherAtCreation", query=Range(**{field: {"gte": min, "lt": max}}))


def _get_filter_season(kwargs):
    return Bool(should=[Range(month=month_range) for month_range in _season_to_range(kwargs["season"])])


def _get_filter_weather(kwargs):
    return Nested(
        path="weatherAtCreation",
        query=Q("match_phrase", **{"weatherAtCreation.description": kwargs["weather"]}),
    )


def _get_fulltext_search_document(query):
    return PhotoDocument.search().query(
        Bool(
            should=[
                Match(
                    **{
                        "name.keyword": {
                            "query": query,
                            "fuzziness": "AUTO",
                            "boost": 5,
                        }
                    }
                ),
                MultiMatch(
                    query=query,
                    fields=(
                        "labels_de^3",
                        "labels_en^3",
                        "labels_de.stemmed^3",
                        "labels_en.stemmed^3",
                        "name.ngram_reverse^0",
                        "name.ngram",
                    ),
                ),
                MatchPhrase(description=query),
                Wildcard(**{"originalCreationDate.formatted": query + "*"}),
            ]
        )
    )
