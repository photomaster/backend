from unittest import TestCase
from unittest.mock import patch

from django.utils import translation

from photo.locations.elastic.autocomplete import AutoCompletion


class AutoCompletionTestCase(TestCase):
    def setUp(self):
        self.mocked_suggestions = ["Spaceship", "Space Force", "Space Shuttle", "SpaceX"]
        self.fetch_suggestions_mock = patch(
            "photo.locations.elastic.autocomplete.AutoCompletion._fetch_suggestions",
            return_value=self.mocked_suggestions,
        )
        self.autocompletion = AutoCompletion()

    def test_get_suggestions(self):
        with self.fetch_suggestions_mock as fetch_suggestions_mock:
            suggestions = self.autocompletion.get_suggestions("name", "Space")

            fetch_suggestions_mock.assert_called_once()
            self.assertEqual(self.mocked_suggestions, suggestions)

    def test_get_suggestions__caches_results(self):
        with self.fetch_suggestions_mock as fetch_suggestions_mock:
            self.autocompletion.get_suggestions("name", "SpaceX")
            self.autocompletion.get_suggestions("name", "SpaceX")
            self.autocompletion.get_suggestions("name", "SpaceX")

            fetch_suggestions_mock.assert_called_once()

    def test_get_suggestions__invalid_field_fallback(self):
        with self.fetch_suggestions_mock as fetch_suggestions_mock:
            self.autocompletion.get_suggestions("strange_field", "foobar")

            fetch_suggestions_mock.assert_called_once_with("name", "foobar")

    def test_get_suggestions__callable_with_localized_labels__default_language(self):
        with self.fetch_suggestions_mock as fetch_suggestions_mock:
            self.autocompletion.get_suggestions("labels", "foobar")

            fetch_suggestions_mock.assert_called_once_with("labels_en", "foobar")

    def test_get_suggestions__callable_with_localized_labels__overridden_language(self):
        with self.fetch_suggestions_mock as fetch_suggestions_mock:
            try:
                translation.activate("de")
                self.autocompletion.get_suggestions("labels", "foobar")

                fetch_suggestions_mock.assert_called_once_with("labels_de", "foobar")
            finally:
                translation.activate("en")

    def test_get_suggestions__callable_with_other_photodata_fields(self):
        with self.fetch_suggestions_mock as fetch_suggestions_mock:
            self.autocompletion.get_suggestions("photodata.model", "iPhone 5")

            fetch_suggestions_mock.assert_called_once_with("photodata.model", "iPhone 5")
