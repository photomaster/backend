from unittest.mock import patch, MagicMock

from django.test import TestCase
from django.utils import translation

from photo.locations.models import Label


def _patch_translation_client__api_response(client):
    def mock_translate(key, **kwargs):
        return {"translatedText": f"{key}_{kwargs['source_language']}->{kwargs['target_language']}"}

    client.return_value.translate = mock_translate


@patch("google.cloud.translate_v2.Client")
class DecoratorTest(TestCase):
    def test_auto_translation__en_to_de(self, translate_client):
        _patch_translation_client__api_response(translate_client)

        with self.env:
            label = Label.objects.create(title_en="foo")
            self.assertEqual(label.title_de, "foo_en->de")

    def test_auto_translation__de_to_en(self, translate_client):
        _patch_translation_client__api_response(translate_client)

        with self.env:
            label = Label.objects.create(title_de="bar")
            self.assertEqual(label.title_en, "bar_de->en")

    def test_auto_translation__default_language(self, translate_client):
        _patch_translation_client__api_response(translate_client)

        with self.env:
            label = Label.objects.create(title="english")
            self.assertEqual(label.title_en, label.title)
            self.assertEqual(label.title_de, "english_en->de")

    def test_auto_translation__overridden_locale(self, translate_client):
        _patch_translation_client__api_response(translate_client)

        with translation.override("de"):
            with self.env:
                label = Label.objects.create(title="german")
                self.assertEqual(label.title, label.title_de)
                self.assertEqual(label.title_en, "german_de->en")

    def test_auto_translation__api_error(self, translate_client):
        mock = MagicMock()
        mock.translate.side_effect = ConnectionError("error establishing connection to service")
        translate_client.return_value = mock

        with self.env:
            label = Label.objects.create(title_en="foo")
            self.assertIsNone(label.title_de, "if translation api returns error do not modify the localized field")

    def test_auto_translation__no_api_key_set(self, translate_client):
        _patch_translation_client__api_response(translate_client)

        label = Label.objects.create(title_en="foo")
        self.assertIsNone(label.title_de, "if no google api key is set do not translate anything")
        translate_client.translate.assert_not_called()

    def setUp(self):
        self.env = patch.dict("os.environ", {"GOOGLE_APPLICATION_CREDENTIALS": "fake.key"})

    def tearDown(self):
        Label.objects.all().delete()
