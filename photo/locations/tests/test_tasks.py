from unittest.mock import patch

from django.test import TestCase

from photo.locations.models import Photo, PhotoData
from photo.locations.tasks import photo_post_create_async, photo_post_update_async, photodata_post_update_async


class AsyncHookMock:
    def apply_async(self, *args, **kwargs):
        pass


class CreationHooksTestCase(TestCase):
    @patch("photo.locations.tasks.update_current_weather", return_value=AsyncHookMock())
    @patch("photo.locations.tasks.set_weather_at_creation", return_value=AsyncHookMock())
    @patch("photo.locations.tasks.send_photo_to_imgsim", return_value=AsyncHookMock())
    @patch("photo.locations.tasks.update_photo_index_item", return_value=AsyncHookMock())
    def test_photo_post_create_async__calls_services(
        self, update_photo_index_item, send_photo_to_imgsim, set_weather_at_creation, update_current_weather
    ):
        photo = Photo(pk=1)
        services = (
            update_photo_index_item,
            send_photo_to_imgsim,
            set_weather_at_creation,
            update_current_weather,
        )

        photo_post_create_async(photo)

        for service in services:
            with self.subTest(service):
                service.apply_async.assert_called_once_with(args=[photo.id])

    @patch("photo.locations.tasks.update_photo_index_item", return_value=AsyncHookMock())
    def test_photo_post_update_async__calls_services(self, update_photo_index_item):
        photo = Photo(pk=1)

        photo_post_update_async(photo)

        update_photo_index_item.apply_async.assert_called_once_with(args=[photo.id])

    @patch("photo.locations.tasks.update_photo_index_item", return_value=AsyncHookMock())
    def test_photodata_post_update_async__calls_index_update(self, update_photo_index_item):
        photo = Photo.objects.create(pk=1)
        photodata = PhotoData(photo=photo)

        photodata_post_update_async(photodata)

        update_photo_index_item.apply_async.assert_called_once_with(args=[photo.id])
