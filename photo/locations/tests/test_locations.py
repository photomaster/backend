import json
from unittest import mock

from django.contrib.gis.geos import Point
from django.test import TestCase

from photo.locations.models import Photo, Location
from photo.locations.service.location import generate_location_clusters, location_name_to_gps


class MockResponse:
    def __init__(self, status_code, json_data=""):
        self.json_data = json_data
        self.status_code = status_code

    def json(self):
        return json.loads(self.json_data)


class LocationServiceTest(TestCase):
    def test_generate_location_clusters(self):
        Photo.objects.create(name="Müllner Steg 1", guid="mülln-1", geo=Point(13.038872478576812, 47.80479609190642))
        Photo.objects.create(name="Müllner Steg 2", guid="mülln-2", geo=Point(13.038529, 47.805246))
        Photo.objects.create(name="Müllner Steg 3", guid="mülln-3", geo=Point(13.037470, 47.804678))
        Photo.objects.create(name="Salzburg Hauptbahnhof", guid="salzburg-1", geo=Point(13.062387, 47.819532))
        Photo.objects.create(name="irgendwo im Golf von Oman", guid="oman-1", geo=Point(47.819532, 13.062387))

        locations = generate_location_clusters()

        self.assertEqual(Location.objects.count(), 3, "Should generate three clusters")
        self.assertEqual(locations.count(), 3, "Should return newly generated location queryset")

        self.assertTrue(Photo.objects.get(guid="mülln-1").cluster.photo_set.count() == 3)
        self.assertTrue(Photo.objects.get(guid="salzburg-1").cluster.photo_set.count() == 1)
        self.assertTrue(Photo.objects.get(guid="oman-1").cluster.photo_set.count() == 1)

        self.assertTrue(
            Photo.objects.get(guid="mülln-1").cluster
            == Photo.objects.get(guid="mülln-2").cluster
            == Photo.objects.get(guid="mülln-3").cluster,
            "photos at Müllner Steg should be assigned to the same location",
        )
        self.assertTrue(
            Photo.objects.get(guid="salzburg-1").cluster != Photo.objects.get(guid="mülln-1").cluster,
            "photos with a certain distance between should not be assigned to the same location",
        )
        self.assertTrue(
            Photo.objects.get(guid="salzburg-1").cluster != Photo.objects.get(guid="oman-1").cluster,
            "photos with a certain distance between should not be assigned to the same location",
        )

    @mock.patch("requests.get")
    def test_location_name_to_gps(self, mock_requests_get):
        from django.core.cache import cache

        try:
            mock_requests_get.return_value = MockResponse(
                status_code=200, json_data='[{"place_id":42,"display_name":"Bavaria","lat":"47.55", "lon":"12.96"}]'
            )
            lat, long = location_name_to_gps("Bavaria")
            self.assertEqual(lat, 47.55)
            self.assertEqual(long, 12.96)
        finally:
            cache.delete("location_nomination_Bavaria")

    @mock.patch("requests.get")
    def test_location_name_to_gps__cache_result(self, mock_requests_get):
        from django.core.cache import cache

        try:
            mock_requests_get.return_value = MockResponse(
                status_code=200, json_data='[{"place_id":42,"display_name":"Bavaria","lat":"47.55", "lon":"12.96"}]'
            )
            location_name_to_gps("Bavaria")
            self.assertIsNotNone(
                cache.get("location_nomination_Bavaria"), "should write retrieved gps coordinates to cache"
            )
        finally:
            cache.delete("location_nomination_Bavaria")

    @mock.patch("requests.get")
    def test_location_name_to_gps__loads_from_cache(self, mock_requests_get):
        from django.core.cache import cache

        try:
            cache.set("location_nomination_Bavaria", "47.55:12.96")
            coords = location_name_to_gps("Bavaria")

            mock_requests_get.assert_not_called()
            self.assertEqual(
                coords,
                (
                    47.55,
                    12.96,
                ),
            )
        finally:
            cache.delete("location_nomination_Bavaria")

    @mock.patch("requests.get")
    def test_location_name_to_gps__multiple_responses(self, mock_requests_get):
        from django.core.cache import cache

        try:
            mock_requests_get.return_value = MockResponse(
                status_code=200,
                json_data='[\
                {"place_id":42,"display_name":"Bavaria","lat":"12.55", "lon":"55.96"},\
                {"place_id":42,"display_name":"Bavaria 2","lat":"13.55", "lon":"50.96"},\
                {"place_id":42,"display_name":"Bavaria 3","lat":"14.55", "lon":"11.96"}]',
            )

            self.assertEqual(
                location_name_to_gps("Bavaria"),
                (12.55, 55.96),
                "should return lat and lon retrieved by the first item of the api response",
            )
        finally:
            cache.delete("location_nomination_Bavaria")

    @mock.patch("requests.get")
    def test_location_name_to_gps__api_error(self, mock_requests_get):
        from django.core.cache import cache

        try:
            mock_requests_get.return_value = MockResponse(status_code=500)

            self.assertRaises(
                RuntimeError, lambda _: location_name_to_gps("Bavaria"), "should raise error if api returns error"
            )
            self.assertIsNone(cache.get("location_nomination_Bavaria"), "should not cache erroneous api responses")
        finally:
            cache.delete("location_nomination_Bavaria")

    @mock.patch("requests.get")
    def test_location_name_to_gps__empty_api_response(self, mock_requests_get):
        from django.core.cache import cache

        try:
            mock_requests_get.return_value = MockResponse(status_code=200, json_data="[]")

            self.assertRaises(
                RuntimeError,
                lambda _: location_name_to_gps("Bavaria"),
                "should raise error if api found no suiting coordinates",
            )
            self.assertFalse(
                cache.get("location_nomination_Bavaria"), "cache should remember that a location could not be resolved"
            )
        finally:
            cache.delete("location_nomination_Bavaria")
