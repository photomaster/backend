from datetime import datetime
from unittest import TestCase
from unittest.mock import patch

from django.contrib.gis.geos import Point
from django.utils import timezone

from photo.locations.models import Photo, WeatherState, User
from photo.locations.service.weather import add_weather_at_creation, update_current_weather


def _patch_weather_api_response(client):
    client.return_value = {
        "coord": {"lon": -122.08, "lat": 37.39},
        "weather": [{"id": 800, "main": "Clear", "description": "clear sky", "icon": "01d"}],
        "base": "stations",
        "main": {
            "temp": 282.55,
            "feels_like": 281.86,
            "temp_min": 280.37,
            "temp_max": 284.26,
            "pressure": 1023,
            "humidity": 100,
        },
        "visibility": 16093,
        "wind": {"speed": 1.5, "deg": 350},
        "clouds": {"all": 1},
        "dt": 1560350645,
        "sys": {
            "type": 1,
            "id": 5122,
            "message": 0.0139,
            "country": "US",
            "sunrise": 1560343627,
            "sunset": 1560396563,
        },
        "timezone": -25200,
        "id": 420006353,
        "name": "Mountain View",
        "cod": 200,
    }


@patch("photo.locations.service.weather._fetch_api_data")
class WeatherServiceUpdateCurrentWeatherTest(TestCase):
    def test_update_current_weather__no_geo_data(self, weather_api_mock):
        photo = Photo()
        update_current_weather(photo)

        self.assertIsNone(photo.currentWeather)
        weather_api_mock.assert_not_called()

    def test_update_current_weather__current_weather_recently_fetched(self, weather_api_mock):
        current_weather = WeatherState(updated_at=timezone.now() - timezone.timedelta(minutes=30))
        photo = Photo(geo=Point(x=12, y=42), currentWeather=current_weather)

        update_current_weather(photo)

        self.assertEqual(photo.currentWeather, current_weather)
        weather_api_mock.assert_not_called()

    def test_update_current_weather__current_weather_recently_fetched__parameter(self, weather_api_mock):
        current_weather = WeatherState(updated_at=timezone.now() - timezone.timedelta(hours=12))
        photo = Photo(geo=Point(x=12, y=42), currentWeather=current_weather)

        update_current_weather(photo, 15)

        self.assertEqual(photo.currentWeather, current_weather)
        weather_api_mock.assert_not_called()

    def test_update_current_weather__current_weather(self, weather_api_mock):
        _patch_weather_api_response(weather_api_mock)

        current_weather = WeatherState(updated_at=(timezone.now() - timezone.timedelta(hours=36)))
        photo = Photo(geo=Point(x=12, y=42), currentWeather=current_weather)

        updated_weather = update_current_weather(photo)

        weather_api_mock.assert_called_with(12, 42)
        self.assertEqual(updated_weather.temperature, 282.55)
        self.assertEqual(updated_weather.description, "clear sky")
        self.assertEqual(updated_weather.locationName, "Mountain View")
        self.assertEqual(updated_weather.locationId, 420006353)
        self.assertEqual(updated_weather.icon, "01d")

    def test_update_current_weather__current_weather__update_existing_weather_state(self, weather_api_mock):
        _patch_weather_api_response(weather_api_mock)

        current_weather = WeatherState(updated_at=(timezone.now() - timezone.timedelta(hours=36)))
        photo = Photo(geo=Point(x=12, y=42), currentWeather=current_weather)

        updated_weather = update_current_weather(photo)

        self.assertEqual(
            updated_weather, current_weather, "if the currentWeather is set all values should be replaced in place"
        )
        self.assertEqual(
            photo.currentWeather, current_weather, "if a photo has a currentWeather not new object should be created"
        )

    def test_update_current_weather__current_weather__create_new_weather_state(self, weather_api_mock):
        _patch_weather_api_response(weather_api_mock)

        photo = Photo.objects.create(geo=Point(x=12, y=42))

        updated_weather = update_current_weather(photo)

        self.assertEqual(
            photo.currentWeather, updated_weather, "if no weather is set a new one should be created and assigned"
        )


@patch("photo.locations.service.weather._fetch_api_data")
class WeatherServiceTest(TestCase):
    @staticmethod
    def tearDownClass():
        Photo.objects.all().delete()

    def test_add_weather_at_creation_geo_missing(self, mock_fetch_api_data):
        photo = Photo(originalCreationDate=timezone.now())
        photo.save()
        self.assertIsInstance(add_weather_at_creation(photo), WeatherState)
        self.assertIsInstance(photo.weatherAtCreation, WeatherState)
        mock_fetch_api_data.assert_not_called()

    def test_add_weather_at_creation_geo_empty_coordinates(self, mock_fetch_api_data):
        photo = Photo(originalCreationDate=timezone.now(), geo=Point())
        photo.save()

        self.assertIsInstance(add_weather_at_creation(photo), WeatherState)
        self.assertIsInstance(photo.weatherAtCreation, WeatherState)
        mock_fetch_api_data.assert_not_called()

    def test_add_weather_at_creation_originalCreationDate_already_set(self, mock_fetch_api_data):
        weather_at_creation = WeatherState()
        photo = Photo(
            geo=Point(x=15, y=15),
            originalCreationDate=datetime.fromisoformat("2000-01-01"),
            weatherAtCreation=weather_at_creation,
        )

        self.assertIsInstance(add_weather_at_creation(photo), WeatherState)
        self.assertEqual(
            photo.weatherAtCreation, weather_at_creation, "should not change an existing weatherAtCreation"
        )
        mock_fetch_api_data.assert_not_called()

    def test_add_weather_at_creation_originalCreationDate_not_today(self, mock_fetch_api_data):
        photo = Photo(geo=Point(x=15, y=15), originalCreationDate=datetime.fromisoformat("2000-01-01"))
        photo.save()

        self.assertIsInstance(add_weather_at_creation(photo), WeatherState)
        self.assertIsInstance(photo.weatherAtCreation, WeatherState)
        mock_fetch_api_data.assert_not_called()

    def test_add_weather_at_creation(self, mock_fetch_api_data):
        _patch_weather_api_response(mock_fetch_api_data)

        user = User.objects.create(username="user", password="1234", email="test@test.weather.de")
        photo = Photo(geo=Point(x=41, y=13), originalCreationDate=timezone.now(), user=user)
        photo.save()

        weather_status = add_weather_at_creation(photo)
        mock_fetch_api_data.assert_called_once_with(photo.geo.x, photo.geo.y)

        self.assertIsInstance(weather_status, WeatherState)
        self.assertEqual(weather_status.user, photo.user)
        self.assertEqual(weather_status.temperature, 282.55)
        self.assertEqual(weather_status.description, "clear sky")
        self.assertEqual(weather_status.locationName, "Mountain View")
        self.assertEqual(weather_status.locationId, 420006353)
        self.assertEqual(weather_status.icon, "01d")

        self.assertEqual(weather_status.photo_creation, photo)
        self.assertEqual(photo.weatherAtCreation, weather_status)
