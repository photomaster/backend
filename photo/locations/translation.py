from django.utils.translation import gettext as _
from modeltranslation.translator import register, TranslationOptions

from .models import Label


@register(Label)
class LabelTranslationOptions(TranslationOptions):
    fields = ("title",)


# the sole purpose of the following code is to have one static translation
# key so ./manage.py makemessages generates a .po file
_("hello world")
