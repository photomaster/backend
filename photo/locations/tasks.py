from celery import shared_task
from django_db_geventpool.utils import close_connection
import logging

from photo.celery import app
from photo.locations.elastic.documents import PhotoDocument
from photo.locations.grpc.image_similarity import notify_creation
from photo.locations.models import Photo, PhotoData
from photo.locations.service import weather, photo as photo_service
from photo.locations.service.photo import SaveSearchException
from photo.locations.service.weather import add_weather_at_creation


def photodata_post_update_async(photodata: PhotoData):
    if photodata and photodata.photo:
        update_photo_index_item.apply_async(args=[photodata.photo.id])


def photo_post_create_async(instance: Photo):
    update_current_weather.apply_async(args=[instance.id])
    set_weather_at_creation.apply_async(args=[instance.id])
    send_photo_to_imgsim.apply_async(args=[instance.id])
    update_photo_index_item.apply_async(args=[instance.id])


def photo_post_create_sync(photo: Photo):
    _update_photo_labels(photo)
    _extract_exif_data(photo)
    _rotate_image(photo)
    add_weather_at_creation(photo)


def photo_post_update_async(photo: Photo):
    update_photo_index_item.apply_async(args=[photo.id])


def photo_post_update_sync(photo: Photo):
    add_weather_at_creation(photo)


@app.task
@close_connection
def update_map_clusters():
    from photo.locations.service.map import generate_detail_clusters

    logging.debug("Updating map clusters....")
    generate_detail_clusters()


@shared_task(autoretry_for=(Exception,), retry_kwargs={"max_retries": 5}, retry_backoff=True)
@close_connection
def set_weather_at_creation(photo_id):
    logging.debug(f"Task: update weatherAtCreation of photo {photo_id}")

    photo = Photo.objects.get(pk=photo_id)
    add_weather_at_creation(photo)


@shared_task(autoretry_for=(Exception,), retry_kwargs={"max_retries": 3})
@close_connection
def send_photo_to_imgsim(photo_id):
    logging.debug(f"Task: Sending photo {photo_id} to imgsim")

    photo = Photo.objects.get(pk=photo_id)
    notify_creation(photo)


@shared_task
@close_connection
def update_photo_index_item(photo_id):
    logging.debug(f"Task: Updating elastic search index item of photo {photo_id}")

    photo = Photo.objects.get(pk=photo_id)
    if photo.visibility == Photo.Visibility.PUBLIC:
        PhotoDocument().update(thing=photo)
    else:
        PhotoDocument().delete(id=photo.id, ignore=(404,))


@shared_task
@close_connection
def update_current_weather(photo_id):
    logging.debug(f"Task: Weather Update of photo {photo_id}")
    photo = Photo.objects.get(pk=photo_id)

    weather.update_current_weather(photo)


def _extract_exif_data(photo: Photo):
    from exif import Image as ExifImage

    photodata = None
    try:
        if not photo.image:
            return

        with open(photo.image.path, "rb") as exif_img:
            img = ExifImage(exif_img)
            if not img.has_exif:
                return

            photodata = PhotoData.objects.create(
                user=photo.user,
                photo=photo,
                make=img.get("make"),
                model=img.get("model"),
                lensMake=img.get("lens_make"),
                lensModel=img.get("lens_model"),
                focalLength=img.get("focal_length"),
                fNumber=img.get("f_number"),
                whiteBalance=img.get("white_balance"),
                lightSource=img.get("light_source"),
                exposureTime=img.get("exposure_time"),
                exposureProgram=img.get("exposure_program"),
                exposureMode=img.get("exposure_mode"),
                isoSpeed=img.get("iso_speed"),
                flash=img.get("flash"),
                pixelXDimension=img.get("pixel_x_dimension"),
                pixelYDimension=img.get("pixel_y_dimension"),
                orientation=img.get("orientation"),
                software=img.get("software"),
            )
    except Exception as e:
        logging.error(f"Could not extract exif data from image {photo.id}", e)
    finally:
        if not photodata:
            PhotoData.objects.create(user=photo.user, photo=photo)


def _rotate_image(photo: Photo):
    from PIL import Image
    from exif import Image as ExifImage

    if not photo.image:
        return

    try:
        with open(photo.image.path, "rb") as exif_img:
            img = ExifImage(exif_img)
            if not img.has_exif:
                return

            orientation = img.orientation

        img = Image.open(photo.image.path)
        if orientation == 6:
            img = img.rotate(-90, expand=True)
        elif orientation == 8:
            img = img.rotate(90, expand=True)
        elif orientation == 3:
            img = img.rotate(180, expand=True)
        elif orientation == 2:
            img = img.transpose(Image.FLIP_LEFT_RIGHT)
        elif orientation == 5:
            img = img.rotate(-90, expand=True).transpose(Image.FLIP_LEFT_RIGHT)
        elif orientation == 7:
            img = img.rotate(90, expand=True).transpose(Image.FLIP_LEFT_RIGHT)
        elif orientation == 4:
            img = img.rotate(180, expand=True).transpose(Image.FLIP_LEFT_RIGHT)

        image_data = list(img.getdata())
        # write data to new image in order to remove all metadata
        image_new = Image.new(img.mode, img.size)
        image_new.putdata(image_data)

        image_new.save(photo.image.path)
    except Exception as e:
        logging.warning("ROTATE ERROR", e)


def _update_photo_labels(photo: Photo):
    logging.debug(f"Task: Update labels of photo {photo.id}")

    try:
        photo_service.classify(photo)
    except SaveSearchException as e:
        photo.delete()
        logging.info("prevented upload of inappropriate content!", e.detail, photo.image.path)
        raise e
    except Exception as e:
        logging.error(e)
