"""
WSGI config for photo project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/
"""

import logging
import os

from django.core.wsgi import get_wsgi_application
from prometheus_client import multiprocess
from psycogreen.gevent import patch_psycopg

settings_module = "photo.config.settings.production"
os.environ.setdefault("DJANGO_SETTINGS_MODULE", settings_module)


def child_exit(server, worker):
    multiprocess.mark_process_dead(worker.pid)


def post_fork(server, worker):
    patch_psycopg()
    worker.log.info("Made Psycopg2 Green")


logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s %(name)s %(levelname)-8s %(message)s",
)
application = get_wsgi_application()
