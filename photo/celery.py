import os

from celery import Celery

settings_module = (
    "photo.config.settings.development" if os.getenv("DEBUG", False) else "photo.config.settings.production"
)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", settings_module)

app = Celery("photo")
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()
