#!/usr/bin/env bash
set -e

./wait-for-it.sh -h "$POSTGRES_HOST" -p 5432 -t 30

if [[ "$@" == "celery" ]]
then
  echo "Starting celery workers..."
  celery -A photo.celery worker -l info -B
  return 0
else
  python manage.py migrate
  python manage.py create_elastic_index

  if [[ -n "$USE_FIXTURES" ]]
  then
    python manage.py loaddata e2e.yaml
  fi

  if [[ "$@" == "dev" ]]
  then
    echo "Starting in development mode..."
    python manage.py runserver 0.0.0.0:8000
  else
    echo "Starting in production mode..."
    export prometheus_multiproc_dir=/tempfs
    gunicorn --bind=0.0.0.0:8000 --worker-class=gevent --worker-connections=250 --workers=3 photo.wsgi
  fi
fi

