all: proto docker-image

.PHONY: proto translations lint lint-fix

proto:
	git submodule update --remote
	python -m grpc_tools.protoc -I proto --python_out=photo/locations/grpc/ --grpc_python_out=photo/locations/grpc proto/image.proto
	cd photo/locations/grpc && sed -i -r 's/import (.+_pb2.*)/from . import \1/g' *_pb2*.py

docker-image:
	docker-compose build

test:
	docker-compose exec web bash -c "python ./manage.py test"

coverage:
	docker-compose exec web bash -c "coverage run ./manage.py test && coverage report > report.txt && cat report.txt"

coverage-html:
	docker-compose exec web bash -c "coverage run ./manage.py test > /dev/null 2>&1 && coverage html"

translations: translations-make translations-compile

translations-make:
	docker-compose exec web bash -c "python ./manage.py makemessages --locale en --ignore venv"
	docker-compose exec web bash -c "python ./manage.py makemessages --locale de --ignore venv"
	sudo chown -R $(USER) .locale

translations-compile:
	docker-compose exec web bash -c "python ./manage.py compilemessages"

lint:
	docker-compose exec web bash -c "flake8 --config .flake8 && black . --check"

lint-fix:
	docker-compose exec web black .

security-scan:
	docker-compose exec web pip install bandit && bandit -r photo/ -x */test_*.py -ll

shell:
	docker-compose exec web bash