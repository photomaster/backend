FROM python:3.8
ARG SECRET_KEY=dev
ENV PYTHONUNBUFFERED=1

RUN apt-get update && \
    apt-get install -yf binutils libproj-dev gdal-bin gettext && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /code

COPY requirements.txt .
RUN pip install -r requirements.txt --no-cache-dir

COPY . .

RUN python -m grpc_tools.protoc -I proto --python_out=photo/locations/grpc/ --grpc_python_out=photo/locations/grpc proto/image.proto
RUN cd photo/locations/grpc && sed -i -r 's/import (.+_pb2.*)/from . import \1/g' *_pb2*.py
RUN  mkdir -p var && \
     python manage.py collectstatic --noinput && \
     python manage.py compilemessages

RUN useradd --system --home-dir /code --shell /bin/bash --uid 1001 --no-log-init photomaster
RUN chown -R photomaster /code
USER photomaster

ENTRYPOINT [ "/code/docker-entrypoint.sh" ]

CMD ["prod"]

EXPOSE 8000/tcp